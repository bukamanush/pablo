﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IT.ITTInterviews.Controllers.PgceSecondary
{
    public class PgceSecondaryController : Controller
    {
        //
        // GET: /PgceSecondary/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult StartForm()
        {
            return PartialView();
        }

        public ActionResult ApplicationReviewForm()
        {
            return PartialView();
        }

        public ActionResult ApplicationsForm()
        {
            return PartialView();
        }

        public ActionResult LiteracyForm()
        {
            return PartialView();
        }

        public ActionResult SKTaskForm()
        {
            return PartialView();
        }

        public ActionResult PresentationForm()
        {
            return PartialView();
        }

        public ActionResult TeachersStandardForm()
        {
            return PartialView();
        }

        public ActionResult PmInterviewForm()
        {
            return PartialView();
        }

        public ActionResult DecisionForm()
        {
            return PartialView();
        }
	}
}