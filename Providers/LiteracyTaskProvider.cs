﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using IT.ITTInterviews.Models;

namespace IT.ITTInterviews.Providers
{
    public class LiteracyTaskProvider
    {
        public LiteracyTask Create(LiteracyTask entity)
        {

            var newentity = new LiteracyTask();
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var list = ctx.LiteracyTasks.Where(u => u.ApplicantId == entity.ApplicantId);
                if (!list.Any())
                {
                    newentity = ctx.LiteracyTasks.Add(entity);
                }
                ctx.SaveChanges();
            }
            return newentity;
        }

        public void Update(LiteracyTask entity)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                ctx.Entry(entity).State = EntityState.Modified;
                ctx.SaveChanges();
            }
        }

        public void Remove(LiteracyTask entity)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var find = ctx.LiteracyTasks.Find(Convert.ToInt32(entity.Id));
                ctx.LiteracyTasks.Remove(find);
                ctx.SaveChanges();
            }
        }

        public LiteracyTask GetById(int id)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var find = ctx.LiteracyTasks.Find(id);
                return find;
            }

        }
        
        public LiteracyTask GetByApplicantId(int id)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var find = ctx.LiteracyTasks.Where(m => m.ApplicantId == id);
                return find.FirstOrDefault();
            }

        }

        public List<LiteracyTask> GetAll()
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                return ctx.LiteracyTasks.ToList();

            }

        }
    }
}