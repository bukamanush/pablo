﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IT.ITTInterviews.Models;
using IT.ITTInterviews.Providers;
using Newtonsoft.Json;

namespace IT.ITTInterviews.Providers
{
    public class LiteracyTaskViewer
    {
        public static LiteracyTaskModel Get(LiteracyTask item)
        {
            try
            {
                var newitem = new LiteracyTaskModel
                {
                    Id = item.Id,
                    ApplicantId = Convert.ToInt32(item.ApplicantId),
                    Observer = item.Observer,
                    Subject = item.Subject,
                    SubjectKnowledge = item.SubjectKnowledge,
                    SubjectKnowledgeScore = Convert.ToInt32(item.SubjectKnowledgeScore),
                    LiteracyComment = item.LiteracyComment,
                    LiteracyScore1 = Convert.ToInt32(item.LiteracyScore1),
                    LiteracyScore2 = Convert.ToInt32(item.LiteracyScore2),
                    LiteracyScore3 = Convert.ToInt32(item.LiteracyScore3),
                    LiteracyScore4 = Convert.ToInt32(item.LiteracyScore4),
                    ModifiedBy = item.ModifiedBy,
                    CreatedBy = item.CreatedBy
                };
                return newitem;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static LiteracyTask Create(LiteracyTask entity)
        {
            var mgr = new LiteracyTaskProvider();
            return mgr.Create(entity);
        }

        public static void Update(LiteracyTask entity)
        {
            var mgr = new LiteracyTaskProvider();
            mgr.Update(entity);
        }

        public static void Remove(LiteracyTask entity)
        {
            var mgr = new LiteracyTaskProvider();
            mgr.Remove(entity);
        }

        public static LiteracyTask GetById(int id)
        {
            var mgr = new LiteracyTaskProvider();
            return mgr.GetById(id);
        }

        public static LiteracyTask GetByApplicantId(int id)
        {
            var mgr = new LiteracyTaskProvider();
            return mgr.GetByApplicantId(id);
        }

        public static List<LiteracyTask> GetAll()
        {
            var mgr = new LiteracyTaskProvider();
            return mgr.GetAll();
        }




    }
}
