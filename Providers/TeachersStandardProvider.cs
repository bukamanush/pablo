﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using IT.ITTInterviews.Models;

namespace IT.ITTInterviews.Providers
{
    public class TeachersStandardProvider
    {
        public TeachersStandard Create(TeachersStandard entity)
        {

            var newentity = new TeachersStandard();
            using (var ctx = new ITTInterviewsDBEntities())
            {
                newentity = ctx.TeachersStandards.Add(entity);
                ctx.SaveChanges();
            }
            return newentity;
        }

        public void Update(TeachersStandard entity)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                ctx.Entry(entity).State = EntityState.Modified;
                ctx.SaveChanges();
            }
        }

        public void Remove(TeachersStandard entity)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var find = ctx.TeachersStandards.Find(Convert.ToInt32(entity.Id));
                ctx.TeachersStandards.Remove(find);
                ctx.SaveChanges();
            }
        }

        public TeachersStandard GetById(int id)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var find = ctx.TeachersStandards.Find(id);
                return find;
            }

        }

        public TeachersStandard GetByApplicantId(int id)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var find = ctx.TeachersStandards.Where(m => m.Applicant1Id == id || m.Applicant2Id==id || m.Applicant3Id==id || m.Applicant4Id==id);
                return find.FirstOrDefault();
            }

        }

        public List<TeachersStandard> GetAll()
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                return ctx.TeachersStandards.ToList();

            }

        }
    }
}