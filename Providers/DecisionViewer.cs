﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IT.ITTInterviews.Models;
using IT.ITTInterviews.Providers;
using Newtonsoft.Json;

namespace IT.ITTInterviews.Providers
{
    public class DecisionViewer
    {
        public static DecisionModel Get(Decision item)
        {
            try
            {
                var newitem = new DecisionModel
                {
                    Id = item.Id,
                    ApplicantId = Convert.ToInt32(item.ApplicantId),
                    OverallGrade = Convert.ToInt32(item.OverallGrade),
                    AcceptedConditionally = Convert.ToBoolean(item.AcceptedConditionally),
                    Rejected = Convert.ToBoolean(item.Rejected),
                    Conditions = item.Conditions,
                    RejectionMain = item.RejectionMain,
                    RejectionReason = item.RejectionReason,
                    OtherIssues = item.OtherIssues,
                    ModifiedBy = item.ModifiedBy,
                    CreatedBy = item.CreatedBy
                };
                return newitem;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static Decision Create(Decision entity)
        {
            var mgr = new DecisionProvider();
            return mgr.Create(entity);
        }

        public static void Update(Decision entity)
        {
            var mgr = new DecisionProvider();
            mgr.Update(entity);
        }

        public static void Remove(Decision entity)
        {
            var mgr = new DecisionProvider();
            mgr.Remove(entity);
        }

        public static Decision GetById(int id)
        {
            var mgr = new DecisionProvider();
            return mgr.GetById(id);
        }

        public static Decision GetByApplicantId(int id)
        {
            var mgr = new DecisionProvider();
            return mgr.GetByApplicantId(id);
        }

        public static List<Decision> GetAll()
        {
            var mgr = new DecisionProvider();
            return mgr.GetAll();
        }




    }
}
