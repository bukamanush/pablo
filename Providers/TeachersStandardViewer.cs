﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IT.ITTInterviews.Models;
using IT.ITTInterviews.Providers;
using Newtonsoft.Json;

namespace IT.ITTInterviews.Providers
{
    public class TeachersStandardViewer
    {
        public static TeachersStandardModel Get(TeachersStandard item)
        {
            try
            {
                var newitem = new TeachersStandardModel
                {
                    Id = item.Id,
                    Observer = item.Observer,
                    Applicant1Id = Convert.ToInt32(item.Applicant1Id),
                    Applicant1Name = item.Applicant1Name,
                    Applicant1Commentary1 = item.Applicant1Commentary1,
                    Applicant1Commentary2 = item.Applicant1Commentary2,
                    Applicant1Row1 = Convert.ToInt32(item.Applicant1Row1),
                    Applicant1Row2 = Convert.ToInt32(item.Applicant1Row2),
                    Applicant1Row3 = Convert.ToInt32(item.Applicant1Row3),
                    Applicant1Row4 = Convert.ToInt32(item.Applicant1Row4),
                    Applicant2Id = Convert.ToInt32(item.Applicant2Id),
                    Applicant2Name = item.Applicant2Name,
                    Applicant2Commentary1 = item.Applicant2Commentary1,
                    Applicant2Commentary2 = item.Applicant2Commentary2,
                    Applicant2Row1 = Convert.ToInt32(item.Applicant2Row1),
                    Applicant2Row2 = Convert.ToInt32(item.Applicant2Row2),
                    Applicant2Row3 = Convert.ToInt32(item.Applicant2Row3),
                    Applicant2Row4 = Convert.ToInt32(item.Applicant2Row4),
                    Applicant3Id = Convert.ToInt32(item.Applicant3Id),
                    Applicant3Name = item.Applicant3Name,
                    Applicant3Commentary1 = item.Applicant3Commentary1,
                    Applicant3Commentary2 = item.Applicant3Commentary2,
                    Applicant3Row1 = Convert.ToInt32(item.Applicant3Row1),
                    Applicant3Row2 = Convert.ToInt32(item.Applicant3Row2),
                    Applicant3Row3 = Convert.ToInt32(item.Applicant3Row3),
                    Applicant3Row4 = Convert.ToInt32(item.Applicant3Row4),
                    Applicant4Id = Convert.ToInt32(item.Applicant4Id),
                    Applicant4Commentary1 = item.Applicant4Commentary1,
                    Applicant4Commentary2 = item.Applicant4Commentary2,
                    Applicant4Name = item.Applicant4Name,
                    Applicant4Row1 = Convert.ToInt32(item.Applicant4Row1),
                    Applicant4Row2 = Convert.ToInt32(item.Applicant4Row2),
                    Applicant4Row3 = Convert.ToInt32(item.Applicant4Row3),
                    Applicant4Row4 = Convert.ToInt32(item.Applicant4Row4),
                    ModifiedBy = item.ModifiedBy,
                    CreatedBy = item.CreatedBy
                };
                return newitem;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static TeachersStandard Create(TeachersStandard entity)
        {
            var mgr = new TeachersStandardProvider();
            return mgr.Create(entity);
        }

        public static void Update(TeachersStandard entity)
        {
            var mgr = new TeachersStandardProvider();
            mgr.Update(entity);
        }

        public static void Remove(TeachersStandard entity)
        {
            var mgr = new TeachersStandardProvider();
            mgr.Remove(entity);
        }

        public static TeachersStandard GetById(int id)
        {
            var mgr = new TeachersStandardProvider();
            return mgr.GetById(id);
        }

        public static TeachersStandard GetByApplicantId(int id)
        {
            var mgr = new TeachersStandardProvider();
            return mgr.GetByApplicantId(id);
        }

        public static List<TeachersStandard> GetAll()
        {
            var mgr = new TeachersStandardProvider();
            return mgr.GetAll();
        }




    }
}
