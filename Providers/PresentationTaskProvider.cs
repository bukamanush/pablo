﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using IT.ITTInterviews.Models;

namespace IT.ITTInterviews.Providers
{
    public class PresentationTaskProvider
    {
        public PresentationTask Create(PresentationTask entity)
        {

            var newentity = new PresentationTask();
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var list = ctx.PresentationTasks.Where(u => u.ApplicantId == entity.ApplicantId);
                if (!list.Any())
                {
                    newentity = ctx.PresentationTasks.Add(entity);
                }
                ctx.SaveChanges();
            }
            return newentity;
        }

        public void Update(PresentationTask entity)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                ctx.Entry(entity).State = EntityState.Modified;
                ctx.SaveChanges();
            }
        }

        public void Remove(PresentationTask entity)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var find = ctx.PresentationTasks.Find(Convert.ToInt32(entity.Id));
                ctx.PresentationTasks.Remove(find);
                ctx.SaveChanges();
            }
        }

        public PresentationTask GetById(int id)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var find = ctx.PresentationTasks.Find(id);
                return find;
            }

        }

        public PresentationTask GetByApplicantId(int id)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var find = ctx.PresentationTasks.Where(m => m.ApplicantId == id);
                return find.FirstOrDefault();
            }

        }

        public List<PresentationTask> GetAll()
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                return ctx.PresentationTasks.ToList();

            }

        }
    }
}