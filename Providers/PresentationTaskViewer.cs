﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IT.ITTInterviews.Models;
using IT.ITTInterviews.Providers;
using Newtonsoft.Json;

namespace IT.ITTInterviews.Providers
{
    public class PresentationTaskViewer
    {
        public static PresentationTaskModel Get(PresentationTask item)
        {
            try
            {
                var newitem = new PresentationTaskModel
                {
                    Id = item.Id,
                    ApplicantId = Convert.ToInt32(item.ApplicantId),
                    Observer = item.Observer,
                    Commentary = item.Commentary,
                    Row1Score = Convert.ToInt32(item.Row1Score),
                    Row2Score = Convert.ToInt32(item.Row2Score),
                    Row3Score = Convert.ToInt32(item.Row3Score),
                    Row4Score = Convert.ToInt32(item.Row4Score),
                    ModifiedBy = item.ModifiedBy,
                    CreatedBy = item.CreatedBy
                };
                return newitem;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static PresentationTask Create(PresentationTask entity)
        {
            var mgr = new PresentationTaskProvider();
            return mgr.Create(entity);
        }

        public static void Update(PresentationTask entity)
        {
            var mgr = new PresentationTaskProvider();
            mgr.Update(entity);
        }

        public static void Remove(PresentationTask entity)
        {
            var mgr = new PresentationTaskProvider();
            mgr.Remove(entity);
        }

        public static PresentationTask GetById(int id)
        {
            var mgr = new PresentationTaskProvider();
            return mgr.GetById(id);
        }

        public static PresentationTask GetByApplicantId(int id)
        {
            var mgr = new PresentationTaskProvider();
            return mgr.GetByApplicantId(id);
        }

        public static List<PresentationTask> GetAll()
        {
            var mgr = new PresentationTaskProvider();
            return mgr.GetAll();
        }




    }
}
