﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IT.ITTInterviews.Models;
using IT.ITTInterviews.Providers;
using Newtonsoft.Json;

namespace IT.ITTInterviews.Providers
{
    public class ApplicantViewer
    {
        public static ApplicantModel Get(Applicant item)
        {
            try
            {
                var newitem = new ApplicantModel
                {
                    Id = item.Id,
                    Forename = item.Forename,
                    Surname = item.Surname,
                    Subject = item.Subject,
                    DisplayName = item.Forename + " " + item.Surname,
                    UcasId = item.UcasId,
                    TeachersStandard = Convert.ToBoolean(item.TeachersStandard),
                    Literacy = Convert.ToBoolean(item.Literacy),
                    SKTask = Convert.ToBoolean(item.SKTask),
                    Presentation = Convert.ToBoolean(item.Presentation),
                    InterviewPm = Convert.ToBoolean(item.InterviewPm),
                    Decision = Convert.ToBoolean(item.Decision),
                    Decision2 = Convert.ToBoolean(item.Decision2),
                    //added
                    RejectReason = item.RejectReason,
                    InterviewReject = item.InterviewReject,
                    RejectHighlight = item.RejectHighlight,
                    ReasonToInviteInterview = item.ReasonToInviteInterview
                };
                return newitem;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static Applicant Create(Applicant entity)
        {
            var mgr = new ApplicantProvider();
            return mgr.Create(entity);
        }

        public static void Update(Applicant entity)
        {
            var mgr = new ApplicantProvider();
            mgr.Update(entity);
        }

        public static void Remove(Applicant entity)
        {
            var mgr = new ApplicantProvider();
            mgr.Remove(entity);
        }

        public static Applicant GetById(int id)
        {
            var mgr = new ApplicantProvider();
            return mgr.GetById(id);
        }

        public static List<Applicant> GetByIds(IEnumerable<int> ids)
        {
            var mgr = new ApplicantProvider();
            return mgr.GetByIds(ids);
        }

        public static List<Applicant> GetAll()
        {
            var mgr = new ApplicantProvider();
            return mgr.GetAll();
        }

        public static List<Applicant> GetAllLive()
        {
            var mgr = new ApplicantProvider();
            return mgr.GetAllLive();
        }

        public static List<Applicant> GetAllIncompleteTeachersStandard()
        {
            var mgr = new ApplicantProvider();
            return mgr.GetAllIncompleteTeachersStandard();
        }

        public static List<Applicant> GetAllByName(string name)
        {
            var mgr = new ApplicantProvider();
            return mgr.GetAllByName(name);
        }



    }
}
