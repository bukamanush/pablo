﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using IT.ITTInterviews.Models;

namespace IT.ITTInterviews.Providers
{
    public class ApplicantProvider
    {
        public Applicant Create(Applicant entity)
        {
            try {
                var newentity = new Applicant();
                using (var ctx = new ITTInterviewsDBEntities())
                {

                    newentity = ctx.Applicants.Add(entity);
                    ctx.SaveChanges();
                }
                return newentity;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void Update(Applicant entity)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                ctx.Entry(entity).State = EntityState.Modified;
                ctx.SaveChanges();
            }
        }

        public void Remove(Applicant entity)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var find = ctx.Applicants.Find(Convert.ToInt32(entity.Id));
                ctx.Applicants.Remove(find);
                ctx.SaveChanges();
            }
        }

        public Applicant GetById(int id)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var find = ctx.Applicants.Find(id);
                return find;
            }
        }

        public List<Applicant> GetByIds(IEnumerable<int> ids)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                return ctx.Applicants.Where(a=>ids.Contains(a.Id)).ToList();
            }
        }

        public List<Applicant> GetAllByName(string name)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var find = ctx.Applicants.Where(m=>m.Forename.StartsWith(name) || m.Surname.StartsWith(name));
                return find.ToList();
            }

        }

        public List<Applicant> GetAllIncompleteTeachersStandard()
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var find = ctx.Applicants.Where(m => m.TeachersStandard!=true && m.StatusId==1);
                return find.ToList();
            }

        }
        
        public List<Applicant> GetAll()
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                return ctx.Applicants.ToList();

            }

        }

        public List<Applicant> GetAllLive()
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                return ctx.Applicants.Where(m=>m.StatusId==1).ToList();

            }

        }
    }
}