﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IT.ITTInterviews.Models;
using IT.ITTInterviews.Providers;
using Newtonsoft.Json;

namespace IT.ITTInterviews.Providers
{
    public class PmInterviewViewer
    {
        public static PmInterviewModel Get(PmInterview item)
        {
            try
            {
                var newitem = new PmInterviewModel
                {
                    Id = item.Id,
                    ApplicantId = Convert.ToInt32(item.ApplicantId),
                    Subject = item.Subject,
                    Interviewer1 = item.Interviewer1,
                    Interviewer2 = item.Interviewer2,
                    DateInterview = item.DateInterview!=null ? Convert.ToDateTime(item.DateInterview).ToShortDateString() : null,
                    Question1Score = Convert.ToInt32(item.Question1Score),
                    Q1Notes = item.Q1Notes,
                    Question2Score = Convert.ToInt32(item.Question2Score),
                    Q2Notes = item.Q2Notes,
                    Question3Score = Convert.ToInt32(item.Question3Score),
                    Q3Notes = item.Q3Notes,
                    Question4Score = Convert.ToInt32(item.Question4Score),
                    Q4Notes = item.Q4Notes,
                    Question5Score = Convert.ToInt32(item.Question5Score),
                    Q5Notes = item.Q5Notes,
                    DbsCheckIssues = Convert.ToBoolean(item.DbsCheckIssues),
                    DbsComment = item.DbsComment,
                    IssuesStudying = Convert.ToBoolean(item.IssuesStudying),
                    IssuesStudyingNotes = item.IssuesStudyingNotes,
                    PhysicalHealth = Convert.ToBoolean(item.PhysicalHealth),
                    PhysicalHealthNotes = item.PhysicalHealthNotes,
                    MentalHealth = Convert.ToBoolean(item.MentalHealth),
                    MentalHealthNotes = item.MentalHealthNotes,
                    PersonalCircumstances = Convert.ToBoolean(item.PersonalCircumstances),
                    PersonalCircumstancesNotes = item.PersonalCircumstancesNotes,
                    ReligiousRequirements = Convert.ToBoolean(item.ReligiousRequirements),
                    ReligiousRequirementsNotes = item.ReligiousRequirementsNotes,
                    FailedQtsCourse = Convert.ToBoolean(item.FailedQtsCourse),
                    FailedQtsCourseNotes = item.FailedQtsCourseNotes,
                    DbsFitnessIssues = Convert.ToBoolean(item.DbsFitnessIssues),
                    DbsFitnessIssuesNotes = item.DbsFitnessIssuesNotes,
                    Questions = item.Questions,
                    Answers = item.Answers,
                    ObservationDays = item.ObservationDays,
                    ModifiedBy = item.ModifiedBy,
                    CreatedBy = item.CreatedBy
                };
                return newitem;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static PmInterview Create(PmInterview entity)
        {
            var mgr = new PmInterviewProvider();
            return mgr.Create(entity);
        }

        public static void Update(PmInterview entity)
        {
            var mgr = new PmInterviewProvider();
            mgr.Update(entity);
        }

        public static void Remove(PmInterview entity)
        {
            var mgr = new PmInterviewProvider();
            mgr.Remove(entity);
        }

        public static PmInterview GetById(int id)
        {
            var mgr = new PmInterviewProvider();
            return mgr.GetById(id);
        }

        public static PmInterview GetByApplicantId(int id)
        {
            var mgr = new PmInterviewProvider();
            return mgr.GetByApplicantId(id);
        }

        public static List<PmInterview> GetAll()
        {
            var mgr = new PmInterviewProvider();
            return mgr.GetAll();
        }




    }
}
