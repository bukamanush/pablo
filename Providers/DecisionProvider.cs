﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using IT.ITTInterviews.Models;

namespace IT.ITTInterviews.Providers
{
    public class DecisionProvider
    {
        public Decision Create(Decision entity)
        {

            var newentity = new Decision();
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var list = ctx.Decisions.Where(u => u.ApplicantId == entity.ApplicantId);
                if (!list.Any())
                {
                    newentity = ctx.Decisions.Add(entity);
                }
                ctx.SaveChanges();
            }
            return newentity;
        }

        public void Update(Decision entity)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                ctx.Entry(entity).State = EntityState.Modified;
                ctx.SaveChanges();
            }
        }

        public void Remove(Decision entity)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var find = ctx.Decisions.Find(Convert.ToInt32(entity.Id));
                ctx.Decisions.Remove(find);
                ctx.SaveChanges();
            }
        }

        public Decision GetById(int id)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var find = ctx.Decisions.Find(id);
                return find;
            }

        }

        public Decision GetByApplicantId(int id)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var find = ctx.Decisions.Where(m=>m.ApplicantId==id);
                return find.FirstOrDefault();
            }

        }

        public List<Decision> GetAll()
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                return ctx.Decisions.ToList();

            }

        }
    }
}