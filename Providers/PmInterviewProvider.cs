﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using IT.ITTInterviews.Models;

namespace IT.ITTInterviews.Providers
{
    public class PmInterviewProvider
    {
        public PmInterview Create(PmInterview entity)
        {

            var newentity = new PmInterview();
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var list = ctx.PmInterviews.Where(u => u.ApplicantId == entity.ApplicantId);
                if (!list.Any())
                {
                    newentity = ctx.PmInterviews.Add(entity);
                }
                ctx.SaveChanges();
            }
            return newentity;
        }

        public void Update(PmInterview entity)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                ctx.Entry(entity).State = EntityState.Modified;
                ctx.SaveChanges();
            }
        }

        public void Remove(PmInterview entity)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var find = ctx.PmInterviews.Find(Convert.ToInt32(entity.Id));
                ctx.PmInterviews.Remove(find);
                ctx.SaveChanges();
            }
        }

        public PmInterview GetById(int id)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var find = ctx.PmInterviews.Find(id);
                return find;
            }

        }

        public PmInterview GetByApplicantId(int id)
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                var find = ctx.PmInterviews.Where(m => m.ApplicantId == id);
                return find.FirstOrDefault();
            }

        }

        public List<PmInterview> GetAll()
        {
            using (var ctx = new ITTInterviewsDBEntities())
            {
                return ctx.PmInterviews.ToList();

            }

        }
    }
}