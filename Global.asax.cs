﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using System.Web.Optimization;

namespace IT.ITTInterviews
{
    public class Global : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void Application_BeginRequest(object sender, EventArgs e)
        {
            //HttpContext.Current.Response.Headers.Add("Access-Control-Allow-Origin", "*");

        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            /*if (FormsAuthentication.CookiesSupported != true) return;
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return;
            try
            {
                var formsAuthenticationTicket = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value);
                if (formsAuthenticationTicket == null) return;
                var username = formsAuthenticationTicket.Name;
                var roles = string.Empty;
                var authenticationuser = StMarysUserViewer.FetchStMarysUserAuthenticationUser();
                if (username == authenticationuser.Reference)
                {
                    roles = authenticationuser.Roles;
                }

                HttpContext.Current.User = new GenericPrincipal(
                    new GenericIdentity(username, "Forms"), roles.Split(';'));
            }
            catch (Exception)
            {
                //somehting went wrong
            }*/
        }
    }
}