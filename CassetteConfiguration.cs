using System.Collections.Generic;
using Cassette;
using Cassette.Scripts;
using Cassette.Stylesheets;

namespace IT.ITTInterviews
{
    /// <summary>
    /// Configures the Cassette asset bundles for the web application.
    /// </summary>
    public class CassetteBundleConfiguration : IConfiguration<BundleCollection>
    {
        public void Configure(BundleCollection bundles)
        {
            // TODO: Configure your bundles here...
            // Please read http://getcassette.net/documentation/configuration

            var masterCssFiles = new List<string> { "~/Content/bootstrap.min.css",
                         "~/Content/font-awesome.min.css",
                         "~/Content/weather-icons.min.css",
                          "~/Content/beyond.css",
                          "~/Content/demo.min.css",
                          "~/Content/typicons.min.css",
                          "~/Content/animate.min.css",
                           "~/Content/skins/darkblue.min.css",
                           "~/Content/angucomplete-alt.css",
                            "~/Content/datetimepicker.css",
                            "~/Content/yamm.css",
                             "~/Content/angular-bootstrap-toggle.min.css",
                            "~/Content/template.css" };
            bundles.Add<StylesheetBundle>("MasterCss", masterCssFiles);


            var homeJavaScriptFiles = new List<string> { "~/Scripts/jquery-1.11.3.js",
                   "~/Scripts/jquery-ui.js",
                    "~/Scripts/jquery.jqURL.js",
                     "~/Scripts/bootstrap.js",
                     "~/Scripts/angular.js",
                       "~/Scripts/angular-route.js",
                        "~/Scripts/angular-sanitize.js",
                        "~/Scripts/angulartics.min.js",
                          "~/Scripts/angulartics-ga.min.js",
                           "~/Scripts/moment.js",
                           "~/Scripts/detect.js",
                           "~/Scripts/angucomplete-alt.js",
                           "~/Scripts/angular-bootstrap-toggle.min.js",
                           "~/Scripts/jquery.slimscroll.min.js",
                           "~/Scripts/ui-bootstrap-tpls-0.13.0.min.js",
                           "~/Scripts/angular-base64.min.js",
                           "~/App_Scripts/Misc/ITAnalytics.js",
                           "~/Scripts/angular-linq.min.js",
                           "~/Scripts/datetimepicker.js",
                           "~/App_Scripts/Home/HomeConfig.js",
                           "~/App_Scripts/Home/HomeCtrl.js"
                          
            };
            bundles.Add<ScriptBundle>("HomeScripts", homeJavaScriptFiles);


            var pgeceSecondaryJavaScriptFiles = new List<string> { "~/Scripts/jquery-1.11.3.js",
                   "~/Scripts/jquery-ui.js",
                    "~/Scripts/jquery.jqURL.js",
                     "~/Scripts/bootstrap.js",
                     "~/Scripts/angular.js",
                       "~/Scripts/angular-route.js",
                        "~/Scripts/angular-sanitize.js",
                        "~/Scripts/angulartics.min.js",
                          "~/Scripts/angulartics-ga.min.js",
                           "~/Scripts/moment.js",
                           "~/Scripts/detect.js",
                           "~/Scripts/angucomplete-alt.js",
                           "~/Scripts/angular-bootstrap-toggle.min.js",
                           "~/Scripts/jquery.slimscroll.min.js",
                           "~/Scripts/ui-bootstrap-tpls-0.13.0.min.js",
                           "~/Scripts/angular-base64.min.js",
                           "~/App_Scripts/Misc/ITAnalytics.js",
                           "~/Scripts/angular-linq.min.js",
                           "~/Scripts/datetimepicker.js",
                           "~/App_Scripts/PgceSecondary/PgceSecondaryConfig.js",
                           "~/App_Scripts/PgceSecondary/PgceSecondaryCtrl.js",
                           "~/App_Scripts/PgceSecondary/ApplicationReviewCtrl.js",
                           "~/App_Scripts/PgceSecondary/ApplicationsCtrl.js",
                            "~/App_Scripts/PgceSecondary/TeachersStandardCtrl.js",
                           "~/App_Scripts/PgceSecondary/LiteracyTaskCtrl.js",
                           "~/App_Scripts/PgceSecondary/SKTaskCtrl.js",
                           "~/App_Scripts/PgceSecondary/PresentationTaskCtrl.js",
                           "~/App_Scripts/PgceSecondary/PmInterviewCtrl.js",
                           "~/App_Scripts/PgceSecondary/DecisionCtrl.js",
                           "~/App_Scripts/Models/ApplicantModel.js",
                           "~/App_Scripts/Models/LiteracyTaskModel.js",
                           "~/App_Scripts/Models/PresentationTaskModel.js",
                           "~/App_Scripts/Models/TeachersStandardModel.js",
                           "~/App_Scripts/Models/TsCandidateModel.js",
                           "~/App_Scripts/Models/PmInterviewModel.js",
                           "~/App_Scripts/Models/DecisionModel.js"
                          
            };
            bundles.Add<ScriptBundle>("PgceSecondaryScripts", pgeceSecondaryJavaScriptFiles);
           
        }
    }
}