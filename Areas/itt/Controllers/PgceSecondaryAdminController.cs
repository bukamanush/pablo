﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IT.ITTInterviews.Models;
using IT.ITTInterviews.Providers;
using StMarys.Forms.Viewers;

namespace IT.ITTInterviews.Areas.itt.Controllers
{
    public class AutoComplete
    {
        // ReSharper disable once InconsistentNaming
        public int total_count { get; set; }
        // ReSharper disable once InconsistentNaming
        public bool incomplete_results { get; set; }
        // ReSharper disable once InconsistentNaming
        public ApplicantModel[] items { get; set; }
    }

    public class PgceSecondaryAdminController : Controller
    {
        //
        // GET: /itt/PgceSecondaryAdmin/

        public JsonResult FetchStaffById(string username)
        {

            if (!string.IsNullOrWhiteSpace(username))
            {

                ActiveDirectoryService mgr = new ActiveDirectoryService();
                var staff = ActiveDirectoryViewer.FetchById(username);
                return staff != null ? Json(staff, JsonRequestBehavior.AllowGet) : null;
            }
            return null;
            //if (regNum.Equals("error", StringComparison.OrdinalIgnoreCase))
            //  throw new Exception("argh");



        }

        public JsonResult FetchByName(string q)
        {

            if (!string.IsNullOrWhiteSpace(q))
            {
                var applicantlist = ApplicantViewer.GetAllByName(q);

                if (applicantlist != null && applicantlist.Any())
                {
                    var slist = new List<ApplicantModel>();
                    foreach (var s in applicantlist)
                    {
                        slist.Add(ApplicantViewer.Get(s));                       
                    }

                    var a = new AutoComplete();
                    a.incomplete_results = false;
                    a.total_count = slist.Count;
                    a.items = slist.ToArray();

                    string json = @"{";

                    json += @"""items"":[";

                    foreach (var item in slist)
                    {
                        json += @", {""id"":""" + item.Id + @""", ""label"":""" + item.Forename + @" " + item.Surname + @""", ""value"":""" + item.Forename + @" " + item.Surname + @"""}";
                    }
                    json += "]}";
                    
                    return Json(a, JsonRequestBehavior.AllowGet);
                }
                return null;
            }
            return null;



        }

        public JsonResult Test()
        {
            try
            {
                var result = Utility.ReadUrl("https://maps.googleapis.com/maps/api/distancematrix/json?origins=TW1 4SX, UK&destinations=SM2 5BZ, UK&key=AIzaSyCgDStrO1C4TU2h-Vuh6IM4XpGBX8F8QMQ");

                return null;

            }
            catch (Exception e)
            {
                return null;
            }
        }
	}
}