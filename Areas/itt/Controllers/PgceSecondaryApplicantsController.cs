﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Script.Serialization;
using IT.ITTInterviews.Models;
using IT.ITTInterviews.Providers;
using StMarys.Forms.Model;
using StMarys.Forms.Services;
using StMarys.Forms.Viewers;
using OfficeOpenXml;
using IT.ITTInterviews.Areas.itt.Models;
using System.Text;
using Newtonsoft.Json;

namespace IT.ITTInterviews.Areas.itt.Controllers
{
    public class PgceSecondaryApplicantsController : Controller
    {
        //
        // GET: /itt/PgceSecondaryApplicants/

        public JsonResult AddNewApplicationReview(string newitem)
        {
            var xitem = new JavaScriptSerializer().Deserialize<ApplicantModel>(newitem);
            var c = new Applicant
            {
                ApplicantType = "PGCESecondary",
                Forename = xitem.Forename,
                Surname = xitem.Surname,
                UcasId = xitem.UcasId,
                DisabilityDeclared = false,
                TeachersStandard = false,
                Literacy = false,
                SKTask = false,
                Presentation = false,
                InterviewPm = false,
                Decision = xitem.Decision,
                Subject = xitem.Subject,
                InterviewReject = xitem.InterviewReject,
                RejectReason = xitem.RejectReason,
                RejectHighlight = xitem.RejectHighlight,
                ReasonToInviteInterview = xitem.ReasonToInviteInterview, //Todo not defined on database.
                StatusId = 1,
                DateModified = DateTime.Now,
                ModifiedBy = xitem.ModifiedBy,
                DateCreated = DateTime.Now,
                CreatedBy = xitem.CreatedBy
            };

            var newc = ApplicantViewer.Create(c);
            var msg = new ResponseMessage
            {
                Id = "200",
                Message = newc.Id.ToString(),
                Title = "success"
            };

            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public FileResult ExportData(ExportDataViewModel exportData)
        {
            var applicants = ApplicantViewer.GetByIds(exportData.ApplicantIds);

            using (MemoryStream ms = new MemoryStream())
            using (TextWriter tw = new StreamWriter(ms))
            {
                var csvWriter = new CsvHelper.CsvWriter(tw);
                csvWriter.Configuration.Delimiter = ";";
                csvWriter.WriteRecords(applicants);
                tw.Flush();
                return File(new MemoryStream(ms.ToArray()), "text/csv", "exportedApplicants.csv");
            }                
        }
       
       
        public JsonResult FetchApplicants(string status)
        {

            try
            {
                var list = string.IsNullOrWhiteSpace(status) ? ApplicantViewer.GetAllLive() : ApplicantViewer.GetAll();
                var newlist = new List<ApplicantModel>();
                if (list != null && list.Any())
                {
                    newlist.AddRange(
                        list.OrderBy(m => m.Forename)
                            .ThenBy(m => m.Surname)
                            .Select(ApplicantViewer.Get)
                            .Where(item => item != null));
                }

                return newlist.Any() ? Json(newlist, JsonRequestBehavior.AllowGet) : null;

            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public JsonResult RemoveApplicant(string id)
        {
            try
            {
                var applicant = ApplicantViewer.GetById(Convert.ToInt32(id));
                var ts = TeachersStandardViewer.GetByApplicantId(Convert.ToInt32(id));
                var lt = LiteracyTaskViewer.GetByApplicantId(Convert.ToInt32(id));
                var pt = PresentationTaskViewer.GetByApplicantId(Convert.ToInt32(id));
                var pm = PmInterviewViewer.GetByApplicantId(Convert.ToInt32(id));
                var d = DecisionViewer.GetByApplicantId(Convert.ToInt32(id));

                ApplicantViewer.Remove(applicant);
                TeachersStandardViewer.Remove(ts);
                LiteracyTaskViewer.Remove(lt);
                PresentationTaskViewer.Remove(pt);
                PmInterviewViewer.Remove(pm);
                DecisionViewer.Remove(d);

                var msg = new ResponseMessage { Id = "200", Title = "success", Message = "success" };

                return Json(msg, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return null;
            }
        }

        public JsonResult ArchiveApplicant(string id)
        {
            try
            {
                var applicant = ApplicantViewer.GetById(Convert.ToInt32(id));
                applicant.StatusId = 2;
                ApplicantViewer.Update(applicant);

                var msg = new ResponseMessage { Id = "200", Title = "success", Message = "success" };

                return Json(msg, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return null;
            }
        }

        public JsonResult FetchIncompleteTeachersStandards()
        {

            try
            {
                var list = ApplicantViewer.GetAllIncompleteTeachersStandard();
                var newlist = new List<ApplicantModel>();
                if (list != null && list.Any())
                {
                    newlist.AddRange(
                        list.OrderBy(m => m.Forename)
                            .ThenBy(m => m.Surname)
                            .Select(ApplicantViewer.Get)
                            .Where(item => item != null));
                }

                return newlist.Any() ? Json(newlist, JsonRequestBehavior.AllowGet) : null;

            }
            catch
            {
                return null;
            }
        }

        public JsonResult FetchTeachersStandard(string id)
        {

            try
            {
                var list = TeachersStandardViewer.GetByApplicantId(Convert.ToInt32(id));
                var newlist = TeachersStandardViewer.Get(list);

                return Json(newlist, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return null;
            }
        }

        public JsonResult SaveTeachersStandard(string newitem, string mode)
        {
            try
            {
                var xitem = new JavaScriptSerializer().Deserialize<TeachersStandardModel>(newitem);
                if (mode == "create")
                {
                    var c = new TeachersStandard
                    {
                        Observer = xitem.Observer,
                        DateModified = DateTime.Now,
                        ModifiedBy = xitem.ModifiedBy,
                        DateCreated = DateTime.Now,
                        CreatedBy = xitem.CreatedBy
                    };
                    if (xitem.Applicant1Id != 0)
                    {
                        c.Applicant1Id = xitem.Applicant1Id;
                        c.Applicant1Name = xitem.Applicant1Name;
                        c.Applicant1Commentary1 = xitem.Applicant1Commentary1;
                        c.Applicant1Commentary2 = xitem.Applicant1Commentary2;
                        c.Applicant1Row1 = xitem.Applicant1Row1;
                        c.Applicant1Row2 = xitem.Applicant1Row2;
                        c.Applicant1Row3 = xitem.Applicant1Row3;
                        c.Applicant1Row4 = xitem.Applicant1Row4;
                    }
                    if (xitem.Applicant2Id != 0)
                    {
                        c.Applicant2Id = xitem.Applicant2Id;
                        c.Applicant2Name = xitem.Applicant2Name;
                        c.Applicant2Commentary1 = xitem.Applicant2Commentary1;
                        c.Applicant2Commentary2 = xitem.Applicant2Commentary2;
                        c.Applicant2Row1 = xitem.Applicant2Row1;
                        c.Applicant2Row2 = xitem.Applicant2Row2;
                        c.Applicant2Row3 = xitem.Applicant2Row3;
                        c.Applicant2Row4 = xitem.Applicant2Row4;
                    }
                    if (xitem.Applicant3Id != 0)
                    {
                        c.Applicant3Id = xitem.Applicant3Id;
                        c.Applicant3Name = xitem.Applicant3Name;
                        c.Applicant3Commentary1 = xitem.Applicant3Commentary1;
                        c.Applicant3Commentary2 = xitem.Applicant3Commentary2;
                        c.Applicant3Row1 = xitem.Applicant3Row1;
                        c.Applicant3Row2 = xitem.Applicant3Row2;
                        c.Applicant3Row3 = xitem.Applicant3Row3;
                        c.Applicant3Row4 = xitem.Applicant3Row4;
                    }
                    if (xitem.Applicant4Id != 0)
                    {
                        c.Applicant4Id = xitem.Applicant4Id;
                        c.Applicant4Name = xitem.Applicant4Name;
                        c.Applicant4Commentary1 = xitem.Applicant4Commentary1;
                        c.Applicant4Commentary2 = xitem.Applicant4Commentary2;
                        c.Applicant4Row1 = xitem.Applicant4Row1;
                        c.Applicant4Row2 = xitem.Applicant4Row2;
                        c.Applicant4Row3 = xitem.Applicant4Row3;
                        c.Applicant4Row4 = xitem.Applicant4Row4;
                    }

                    var newc = TeachersStandardViewer.Create(c);
                }
                else
                {
                    var c = TeachersStandardViewer.GetById(Convert.ToInt32(xitem.Id));
                    c.Observer = xitem.Observer;
                    c.ModifiedBy = xitem.ModifiedBy;
                    c.DateModified = DateTime.Now;
                    if (xitem.Applicant1Id != 0)
                    {
                        c.Applicant1Id = xitem.Applicant1Id;
                        c.Applicant1Name = xitem.Applicant1Name;
                        c.Applicant1Commentary1 = xitem.Applicant1Commentary1;
                        c.Applicant1Commentary2 = xitem.Applicant1Commentary2;
                        c.Applicant1Row1 = xitem.Applicant1Row1;
                        c.Applicant1Row2 = xitem.Applicant1Row2;
                        c.Applicant1Row3 = xitem.Applicant1Row3;
                        c.Applicant1Row4 = xitem.Applicant1Row4;
                    }
                    if (xitem.Applicant2Id != 0)
                    {
                        c.Applicant2Id = xitem.Applicant2Id;
                        c.Applicant2Name = xitem.Applicant2Name;
                        c.Applicant2Commentary1 = xitem.Applicant2Commentary1;
                        c.Applicant2Commentary2 = xitem.Applicant2Commentary2;
                        c.Applicant2Row1 = xitem.Applicant2Row1;
                        c.Applicant2Row2 = xitem.Applicant2Row2;
                        c.Applicant2Row3 = xitem.Applicant2Row3;
                        c.Applicant2Row4 = xitem.Applicant2Row4;
                    }
                    if (xitem.Applicant3Id != 0)
                    {
                        c.Applicant3Id = xitem.Applicant3Id;
                        c.Applicant3Name = xitem.Applicant3Name;
                        c.Applicant3Commentary1 = xitem.Applicant3Commentary1;
                        c.Applicant3Commentary2 = xitem.Applicant3Commentary2;
                        c.Applicant3Row1 = xitem.Applicant3Row1;
                        c.Applicant3Row2 = xitem.Applicant3Row2;
                        c.Applicant3Row3 = xitem.Applicant3Row3;
                        c.Applicant3Row4 = xitem.Applicant3Row4;
                    }
                    if (xitem.Applicant4Id != 0)
                    {
                        c.Applicant4Id = xitem.Applicant4Id;
                        c.Applicant4Name = xitem.Applicant4Name;
                        c.Applicant4Commentary1 = xitem.Applicant4Commentary1;
                        c.Applicant4Commentary2 = xitem.Applicant4Commentary2;
                        c.Applicant4Row1 = xitem.Applicant4Row1;
                        c.Applicant4Row2 = xitem.Applicant4Row2;
                        c.Applicant4Row3 = xitem.Applicant4Row3;
                        c.Applicant4Row4 = xitem.Applicant4Row4;
                    }
                    TeachersStandardViewer.Update(c);

                }
                if (xitem.Applicant1Id != 0)
                {
                    var app = ApplicantViewer.GetById(Convert.ToInt32(xitem.Applicant1Id));
                    app.TeachersStandard = true;
                    ApplicantViewer.Update(app);
                }
                if (xitem.Applicant2Id != 0)
                {
                    var app = ApplicantViewer.GetById(Convert.ToInt32(xitem.Applicant2Id));
                    app.TeachersStandard = true;
                    ApplicantViewer.Update(app);
                }
                if (xitem.Applicant3Id != 0)
                {
                    var app = ApplicantViewer.GetById(Convert.ToInt32(xitem.Applicant3Id));
                    app.TeachersStandard = true;
                    ApplicantViewer.Update(app);
                }
                if (xitem.Applicant4Id != 0)
                {
                    var app = ApplicantViewer.GetById(Convert.ToInt32(xitem.Applicant4Id));
                    app.TeachersStandard = true;
                    ApplicantViewer.Update(app);
                }

                var msg = new ResponseMessage
                {
                    Id = "200",
                    Message = xitem.Id.ToString(),
                    Title = "success"
                };

                return Json(msg, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return null;
            }

        }

        public JsonResult FetchApplicant(string id)
        {

            try
            {
                var list = ApplicantViewer.GetById(Convert.ToInt32(id));
                var newlist = ApplicantViewer.Get(list);

                return Json(newlist, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return null;
            }
        }

        public JsonResult FetchLiteracyTask(string id)
        {

            try
            {
                var list = LiteracyTaskViewer.GetByApplicantId(Convert.ToInt32(id));
                var newlist = LiteracyTaskViewer.Get(list);

                return Json(newlist, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return null;
            }
        }

        /*public JsonResult SaveSKTask(string newitem, string mode)
        {
            try
            {
                var xitem = new JavaScriptSerializer().Deserialize<SKTaskModel>(newitem);
                if (mode == "create")
                {
                    var c = new SKTask
                    {
                        ApplicantId = Convert.ToInt32(xitem.ApplicantId),
                        Observer = xitem.Observer,
                        Subject = xitem.Subject,
                        //SKTask
                        SubjectKnowledge = xitem.SubjectKnowledge,
                        SubjectKnowledgeScore = Convert.ToInt32(xitem.SubjectKnowledgeScore),

                        //literacy
                        LiteracyComment = xitem.LiteracyComment,
                        LiteracyScore1 = Convert.ToInt32(xitem.LiteracyScore1),
                        LiteracyScore2 = Convert.ToInt32(xitem.LiteracyScore2),
                        LiteracyScore3 = Convert.ToInt32(xitem.LiteracyScore3),
                        LiteracyScore4 = Convert.ToInt32(xitem.LiteracyScore4),

                        DateModified = DateTime.Now,
                        ModifiedBy = xitem.ModifiedBy,
                        DateCreated = DateTime.Now,
                        CreatedBy = xitem.CreatedBy
                    };
                    var newc = LiteracyTaskViewer.Create(c);
                }
                else
                {
                    var c = LiteracyTaskViewer.GetByApplicantId(Convert.ToInt32(xitem.ApplicantId));
                    c.Observer = xitem.Observer;
                    c.Subject = xitem.Subject;
                    c.SubjectKnowledge = xitem.SubjectKnowledge;
                    c.SubjectKnowledgeScore = Convert.ToInt32(xitem.SubjectKnowledgeScore);
                    c.LiteracyComment = xitem.LiteracyComment;
                    c.LiteracyScore1 = Convert.ToInt32(xitem.LiteracyScore1);
                    c.LiteracyScore2 = Convert.ToInt32(xitem.LiteracyScore2);
                    c.LiteracyScore3 = Convert.ToInt32(xitem.LiteracyScore3);
                    c.LiteracyScore4 = Convert.ToInt32(xitem.LiteracyScore4);
                    c.ModifiedBy = xitem.ModifiedBy;
                    c.DateModified = DateTime.Now;

                    LiteracyTaskViewer.Update(c);

                }

                var app = ApplicantViewer.GetById(Convert.ToInt32(xitem.ApplicantId));
                app.Literacy = true;
                ApplicantViewer.Update(app);

                var msg = new ResponseMessage
                {
                    Id = "200",
                    Message = xitem.ApplicantId.ToString(),
                    Title = "success"
                };

                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                return null;
            }

        }
        */
        public JsonResult SaveLiteracy(string newitem, string mode)
        {
            try
            {
                string saveType;
                //var xitem = JsonConvert.DeserializeObject<LiteracyTaskModel>(newitem); 
                //JsonConvert.Deserialize<dynamic>("{x: 'hello'}");
                    var xitem = new JavaScriptSerializer().Deserialize<LiteracyTaskModel>(newitem);
                /*
                if (xitem.Subject)
                {
                    var saveType = 'SKTask';
                }*/


                if (mode == "create")
                {
                    var c = new LiteracyTask
                    {
                        ApplicantId = Convert.ToInt32(xitem.ApplicantId),
                        Observer = xitem.Observer,
                        //SKtask
                        Subject = xitem.Subject,
                        SubjectKnowledge = xitem.SubjectKnowledge,
                        SubjectKnowledgeScore = Convert.ToInt32(xitem.SubjectKnowledgeScore),
                        //literacy
                        LiteracyComment = xitem.LiteracyComment,
                        LiteracyScore1 = Convert.ToInt32(xitem.LiteracyScore1),
                        LiteracyScore2 = Convert.ToInt32(xitem.LiteracyScore2),
                        LiteracyScore3 = Convert.ToInt32(xitem.LiteracyScore3),
                        LiteracyScore4 = Convert.ToInt32(xitem.LiteracyScore4),

                        DateModified = DateTime.Now,
                        ModifiedBy = xitem.ModifiedBy,
                        DateCreated = DateTime.Now,
                        CreatedBy = xitem.CreatedBy
                    };
                    var newc = LiteracyTaskViewer.Create(c);
                }
                else
                {
                    var c = LiteracyTaskViewer.GetByApplicantId(Convert.ToInt32(xitem.ApplicantId));
                    c.Observer = xitem.Observer;

                    if ( xitem.LiteracyScore1 == -1) //updating SkTask part
                    { 

                        c.Subject = xitem.Subject;
                        c.SubjectKnowledge = xitem.SubjectKnowledge;
                        c.SubjectKnowledgeScore = Convert.ToInt32(xitem.SubjectKnowledgeScore);
                    }
                    else //updating Literacy
                    {
                        c.LiteracyComment = xitem.LiteracyComment;
                        c.LiteracyScore1 = Convert.ToInt32(xitem.LiteracyScore1);
                        c.LiteracyScore2 = Convert.ToInt32(xitem.LiteracyScore2);
                        c.LiteracyScore3 = Convert.ToInt32(xitem.LiteracyScore3);
                        c.LiteracyScore4 = Convert.ToInt32(xitem.LiteracyScore4);
                       
                    }

                    c.ModifiedBy = xitem.ModifiedBy;
                    c.DateModified = DateTime.Now;

                    LiteracyTaskViewer.Update(c);

                }

                var app = ApplicantViewer.GetById(Convert.ToInt32(xitem.ApplicantId));
                if (xitem.Subject == "") //Subject is '' when saving literracy only.
                {
                    app.Literacy = true;
                }
                else
                {
                    app.SKTask = true; //todo create SKTask on applicant.
                }
                //app.Literacy = true;
                ApplicantViewer.Update(app);

                var msg = new ResponseMessage
                {
                    Id = "200",
                    Message = xitem.ApplicantId.ToString(),
                    Title = "success"
                };

                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                return null;
            }

        }

        public JsonResult FetchPresentationTask(string id)
        {

            try
            {
                var list = PresentationTaskViewer.GetByApplicantId(Convert.ToInt32(id));
                var newlist = PresentationTaskViewer.Get(list);

                return Json(newlist, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return null;
            }
        }

        public JsonResult SavePresentationTask(string newitem, string mode)
        {
            try
            {
                var xitem = new JavaScriptSerializer().Deserialize<PresentationTaskModel>(newitem);
                if (mode == "create")
                {
                    var c = new PresentationTask
                    {
                        ApplicantId = Convert.ToInt32(xitem.ApplicantId),
                        Observer = xitem.Observer,
                        Commentary = xitem.Commentary,
                        Row1Score = Convert.ToInt32(xitem.Row1Score),
                        Row2Score = Convert.ToInt32(xitem.Row2Score),
                        Row3Score = Convert.ToInt32(xitem.Row3Score),
                        Row4Score = Convert.ToInt32(xitem.Row4Score),
                        DateModified = DateTime.Now,
                        ModifiedBy = xitem.ModifiedBy,
                        DateCreated = DateTime.Now,
                        CreatedBy = xitem.CreatedBy
                    };
                    var newc = PresentationTaskViewer.Create(c);
                }
                else
                {
                    var c = PresentationTaskViewer.GetByApplicantId(Convert.ToInt32(xitem.ApplicantId));
                    c.Observer = xitem.Observer;
                    c.Commentary = xitem.Commentary;
                    c.Row1Score = Convert.ToInt32(xitem.Row1Score);
                    c.Row2Score = Convert.ToInt32(xitem.Row2Score);
                    c.Row3Score = Convert.ToInt32(xitem.Row3Score);
                    c.Row4Score = Convert.ToInt32(xitem.Row4Score);
                    c.ModifiedBy = xitem.ModifiedBy;
                    c.DateModified = DateTime.Now;

                    PresentationTaskViewer.Update(c);

                }

                var app = ApplicantViewer.GetById(Convert.ToInt32(xitem.ApplicantId));
                app.Presentation = true;
                ApplicantViewer.Update(app);

                var msg = new ResponseMessage
                {
                    Id = "200",
                    Message = xitem.ApplicantId.ToString(),
                    Title = "success"
                };

                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return null;
            }

        }

        public JsonResult FetchPmInterview(string id)
        {

            try
            {
                var list = PmInterviewViewer.GetByApplicantId(Convert.ToInt32(id));
                var newlist = PmInterviewViewer.Get(list);

                return Json(newlist, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return null;
            }
        }

        public JsonResult SavePmInterview(string newitem, string mode)
        {
            try
            {
                var xitem = new JavaScriptSerializer().Deserialize<PmInterviewModel>(newitem);
                if (mode == "create")
                {
                    var c = new PmInterview
                    {
                        ApplicantId = Convert.ToInt32(xitem.ApplicantId),
                        Subject = xitem.Subject,
                        Interviewer1 = xitem.Interviewer1,
                        Interviewer2 = xitem.Interviewer2,
                        DateInterview = DateTime.Now,
                        Question1Score = xitem.Question1Score,
                        Q1Notes = xitem.Q1Notes,
                        Question2Score = xitem.Question2Score,
                        Q2Notes = xitem.Q2Notes,
                        Question3Score = xitem.Question3Score,
                        Q3Notes = xitem.Q3Notes,
                        Question4Score = xitem.Question4Score,
                        Q4Notes = xitem.Q4Notes,
                        Question5Score = xitem.Question5Score,
                        Q5Notes = xitem.Q5Notes,
                        DbsCheckIssues = xitem.DbsCheckIssues,
                        DbsComment = xitem.DbsComment,
                        IssuesStudying = xitem.IssuesStudying,
                        IssuesStudyingNotes = xitem.IssuesStudyingNotes,
                        PhysicalHealth = xitem.PhysicalHealth,
                        PhysicalHealthNotes = xitem.PhysicalHealthNotes,
                        MentalHealth = xitem.MentalHealth,
                        MentalHealthNotes = xitem.MentalHealthNotes,
                        PersonalCircumstances = xitem.PersonalCircumstances,
                        PersonalCircumstancesNotes = xitem.PersonalCircumstancesNotes,
                        ReligiousRequirements = xitem.ReligiousRequirements,
                        ReligiousRequirementsNotes = xitem.ReligiousRequirementsNotes,
                        FailedQtsCourse = xitem.FailedQtsCourse,
                        FailedQtsCourseNotes = xitem.FailedQtsCourseNotes,
                        DbsFitnessIssues = xitem.DbsFitnessIssues,
                        DbsFitnessIssuesNotes = xitem.DbsFitnessIssuesNotes,
                        Questions = xitem.Questions,
                        Answers = xitem.Answers,
                        DateModified = DateTime.Now,
                        ModifiedBy = xitem.ModifiedBy,
                        DateCreated = DateTime.Now,
                        CreatedBy = xitem.CreatedBy,
                        ObservationDays = xitem.ObservationDays
                    };
                    var newc = PmInterviewViewer.Create(c);
                }
                else
                {
                    var c = PmInterviewViewer.GetByApplicantId(Convert.ToInt32(xitem.ApplicantId));
                    c.Subject = xitem.Subject;
                    c.Interviewer1 = xitem.Interviewer1;
                    c.Interviewer2 = xitem.Interviewer2;
                    c.Question1Score = xitem.Question1Score;
                    c.Q1Notes = xitem.Q1Notes;
                    c.Question2Score = xitem.Question2Score;
                    c.Q2Notes = xitem.Q2Notes;
                    c.Question3Score = xitem.Question3Score;
                    c.Q3Notes = xitem.Q3Notes;
                    c.Question4Score = xitem.Question4Score;
                    c.Q4Notes = xitem.Q4Notes;
                    c.Question5Score = xitem.Question5Score;
                    c.Q5Notes = xitem.Q5Notes;
                    c.DbsCheckIssues = xitem.DbsCheckIssues;
                    c.DbsComment = xitem.DbsComment;
                    c.IssuesStudying = xitem.IssuesStudying;
                    c.IssuesStudyingNotes = xitem.IssuesStudyingNotes;
                    c.PhysicalHealth = xitem.PhysicalHealth;
                    c.PhysicalHealthNotes = xitem.PhysicalHealthNotes;
                    c.MentalHealth = xitem.MentalHealth;
                    c.MentalHealthNotes = xitem.MentalHealthNotes;
                    c.PersonalCircumstances = xitem.PersonalCircumstances;
                    c.PersonalCircumstancesNotes = xitem.PersonalCircumstancesNotes;
                    c.ReligiousRequirements = xitem.ReligiousRequirements;
                    c.ReligiousRequirementsNotes = xitem.ReligiousRequirementsNotes;
                    c.FailedQtsCourse = xitem.FailedQtsCourse;
                    c.FailedQtsCourseNotes = xitem.FailedQtsCourseNotes;
                    c.DbsFitnessIssues = xitem.DbsFitnessIssues;
                    c.DbsFitnessIssuesNotes = xitem.DbsFitnessIssuesNotes;
                    c.Questions = xitem.Questions;
                    c.Answers = xitem.Answers;
                    c.ModifiedBy = xitem.ModifiedBy;
                    c.DateModified = DateTime.Now;
                    c.ObservationDays = xitem.ObservationDays;

                    PmInterviewViewer.Update(c);

                }

                var app = ApplicantViewer.GetById(Convert.ToInt32(xitem.ApplicantId));
                app.InterviewPm = true;
                ApplicantViewer.Update(app);

                var msg = new ResponseMessage
                {
                    Id = "200",
                    Message = xitem.ApplicantId.ToString(),
                    Title = "success"
                };

                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return null;
            }

        }

        public JsonResult FetchDecision(string id)
        {

            try
            {
                var list = DecisionViewer.GetByApplicantId(Convert.ToInt32(id));
                var newlist = DecisionViewer.Get(list);

                return Json(newlist, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return null;
            }
        }

        public JsonResult SaveDecision(string newitem, string mode)
        {
            try
            {
                var xitem = new JavaScriptSerializer().Deserialize<DecisionModel>(newitem);
                if (mode == "create")
                {
                    var c = new Decision
                    {
                        ApplicantId = Convert.ToInt32(xitem.ApplicantId),
                        OverallGrade = xitem.OverallGrade,
                        AcceptedConditionally = xitem.AcceptedConditionally,
                        Rejected = xitem.Rejected,
                        Conditions = xitem.Conditions,
                        RejectionMain = xitem.RejectionMain,
                        RejectionReason = xitem.RejectionReason,
                        OtherIssues = xitem.OtherIssues,
                        DateModified = DateTime.Now,
                        ModifiedBy = xitem.ModifiedBy,
                        DateCreated = DateTime.Now,
                        CreatedBy = xitem.CreatedBy
                    };
                    var newc = DecisionViewer.Create(c);
                }
                else
                {
                    var c = DecisionViewer.GetByApplicantId(Convert.ToInt32(xitem.ApplicantId));
                    c.OverallGrade = xitem.OverallGrade;
                    c.AcceptedConditionally = xitem.AcceptedConditionally;
                    c.Rejected = xitem.Rejected;
                    c.Conditions = xitem.Conditions;
                    c.RejectionMain = xitem.RejectionMain;
                    c.RejectionReason = xitem.RejectionReason;
                    c.OtherIssues = xitem.OtherIssues;
                    c.ModifiedBy = xitem.ModifiedBy;
                    c.DateModified = DateTime.Now;

                    DecisionViewer.Update(c);

                }

                var app = ApplicantViewer.GetById(Convert.ToInt32(xitem.ApplicantId));
                app.Decision = true;
                ApplicantViewer.Update(app);

                var msg = new ResponseMessage
                {
                    Id = "200",
                    Message = xitem.ApplicantId.ToString(),
                    Title = "success"
                };

                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return null;
            }

        }
    }
}