﻿using System.Web.Mvc;

namespace IT.ITTInterviews.Areas.itt
{
    public class ittAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "itt";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "itt_default",
                "itt/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}