﻿using System.Collections.Generic;

namespace IT.ITTInterviews.Areas.itt.Models
{
    public class ExportDataViewModel
    {
        public List<int> ApplicantIds { get; set; }
    }
}