﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace IT.ITTInterviews
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
            name: "PGCESecondary",
            url: "pgcesecondary",
            defaults: new { controller = "PgceSecondary", action = "Index", id = UrlParameter.Optional }
        );

            routes.MapRoute(
            name: "PGCESecondary_ApplicationReview",
            url: "pgcesecondary/applicationreview",
            defaults: new { controller = "PgceSecondary", action = "Index", id = UrlParameter.Optional }
        );

            routes.MapRoute(
            name: "PGCESecondary_Applications",
            url: "pgcesecondary/applications",
            defaults: new { controller = "PgceSecondary", action = "Index", id = UrlParameter.Optional }
        );

            routes.MapRoute(
              name: "PGCESecondary_Applications2",
              url: "pgcesecondary/applications/{id}",
              defaults: new { controller = "PgceSecondary", action = "Index", id = UrlParameter.Optional }
          );

            routes.MapRoute(
              name: "PGCESecondary_Literacy",
              url: "pgcesecondary/literacy/{id}",
              defaults: new { controller = "PgceSecondary", action = "Index", id = UrlParameter.Optional }
          );

            routes.MapRoute(
             name: "PGCESecondary_SKTask",
             url: "pgcesecondary/sktask/{id}",
             defaults: new { controller = "PgceSecondary", action = "Index", id = UrlParameter.Optional }
         );

            routes.MapRoute(
              name: "PGCESecondary_Presentation",
              url: "pgcesecondary/presentation/{id}",
              defaults: new { controller = "PgceSecondary", action = "Index", id = UrlParameter.Optional }
          );

            routes.MapRoute(
              name: "PGCESecondary_TeachersStandard",
              url: "pgcesecondary/teachersstandard",
              defaults: new { controller = "PgceSecondary", action = "Index", id = UrlParameter.Optional }
          );

            routes.MapRoute(
              name: "PGCESecondary_TeachersStandard_Id",
              url: "pgcesecondary/teachersstandard/{id}",
              defaults: new { controller = "PgceSecondary", action = "Index", id = UrlParameter.Optional }
          );

            routes.MapRoute(
              name: "PGCESecondary_PmInterview",
              url: "pgcesecondary/pminterview/{id}",
              defaults: new { controller = "PgceSecondary", action = "Index", id = UrlParameter.Optional }
          );

            routes.MapRoute(
              name: "PGCESecondary_Decision",
              url: "pgcesecondary/decision/{id}",
              defaults: new { controller = "PgceSecondary", action = "Index", id = UrlParameter.Optional }
          );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
