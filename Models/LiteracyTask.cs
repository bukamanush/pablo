//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IT.ITTInterviews.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class LiteracyTask
    {
        public int Id { get; set; }
        public Nullable<int> ApplicantId { get; set; }
        public string Observer { get; set; }
        public string Subject { get; set; }
        public string SubjectKnowledge { get; set; }
        public Nullable<int> SubjectKnowledgeScore { get; set; }
        public string LiteracyComment { get; set; }
        public Nullable<int> LiteracyScore1 { get; set; }
        public Nullable<int> LiteracyScore2 { get; set; }
        public Nullable<int> LiteracyScore3 { get; set; }
        public Nullable<int> LiteracyScore4 { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> DateModified { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
        public byte[] timestamp { get; set; }
    }
}
