﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IT.ITTInterviews.Models
{
    public class ApplicantModel
    {
        public int Id { get; set; }
        public string ApplicantType { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public string DisplayName { get; set; }
        public string UcasId { get; set; }
        public bool DisabilityDeclared { get; set; }
        public string Subject { get; set; }
        public string AcademicBackground { get; set; }
        public string NeekSke { get; set; }
        public string PersonalStatementReferences { get; set; }
        public string DaysSchoolObservation { get; set; }
        public string PersonalStatement { get; set; }
        public string Reference1 { get; set; }
        public string Reference1Reaction { get; set; }
        public string Reference2 { get; set; }
        public string Reference2Reaction { get; set; }
        public string Reference3 { get; set; }
        public string Reference3Reaction { get; set; }
        public bool AppropriateReferences { get; set; }
        public string ReferenceComment { get; set; }
        public string ApplicationReviewOutcome { get; set; }
        public string InterviewReject { get; set; }
        public string RejectHighlight { get; set; }
        public string RejectReason { get; set; }
        public string ReviewBy { get; set; }
        public bool TeachersStandard { get; set; }
        public bool Literacy { get; set; }
        public bool SKTask { get; set; }
        public bool Presentation { get; set; }
        public bool InterviewPm { get; set; }
        public bool Decision { get; set; }
        public bool Decision2 { get; set; }
        public int StatusId { get; set; }
        public string ModifiedBy { get; set; }
        public string DateModified { get; set; }
        public string CreatedBy { get; set; }
        public string DateCreated { get; set; }
        public string ReasonToInviteInterview { get; set; }
    }
}