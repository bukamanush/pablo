﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IT.ITTInterviews.Models
{
    public class DecisionModel
    {
        public int Id { get; set; }
        public int ApplicantId { get; set; }
        public int OverallGrade { get; set; }
        public bool AcceptedConditionally { get; set; }
        public bool Rejected { get; set; }
        public string Conditions { get; set; }
        public string RejectionMain { get; set; }
        public string RejectionReason { get; set; }
        public string OtherIssues { get; set; }
        public string ModifiedBy { get; set; }
        public string CreatedBy { get; set; }
        
    }
}