﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IT.ITTInterviews.Models
{
    public class TeachersStandardModel
    {
        public int Id { get; set; }
        public string Observer { get; set; }
        public int Applicant1Id { get; set; }
        public string Applicant1Name { get; set; }
        public string Applicant1Commentary1 { get; set; }
        public string Applicant1Commentary2 { get; set; }
        public int Applicant1Row1 { get; set; }
        public int Applicant1Row2 { get; set; }
        public int Applicant1Row3 { get; set; }
        public int Applicant1Row4 { get; set; }
        public int Applicant2Id { get; set; }
        public string Applicant2Name { get; set; }
        public string Applicant2Commentary1 { get; set; }
        public string Applicant2Commentary2 { get; set; }
        public int Applicant2Row1 { get; set; }
        public int Applicant2Row2 { get; set; }
        public int Applicant2Row3 { get; set; }
        public int Applicant2Row4 { get; set; }
        public int Applicant3Id { get; set; }
        public string Applicant3Name { get; set; }
        public string Applicant3Commentary1 { get; set; }
        public string Applicant3Commentary2 { get; set; }
        public int Applicant3Row1 { get; set; }
        public int Applicant3Row2 { get; set; }
        public int Applicant3Row3 { get; set; }
        public int Applicant3Row4 { get; set; }
        public int Applicant4Id { get; set; }
        public string Applicant4Name { get; set; }
        public string Applicant4Commentary1 { get; set; }
        public string Applicant4Commentary2 { get; set; }
        public int Applicant4Row1 { get; set; }
        public int Applicant4Row2 { get; set; }
        public int Applicant4Row3 { get; set; }
        public int Applicant4Row4 { get; set; }
        public string ModifiedBy { get; set; }
        public string CreatedBy { get; set; }

    }
}