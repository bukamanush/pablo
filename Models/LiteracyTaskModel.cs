﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IT.ITTInterviews.Models
{
    public class LiteracyTaskModel
    {
        public int Id { get; set; }
        public int ApplicantId { get; set; }
        public string Observer { get; set; }
        public string Subject { get; set; }
        public string SubjectKnowledge { get; set; }
        public int SubjectKnowledgeScore { get; set; }
        public string LiteracyComment { get; set; }
        public int LiteracyScore1 { get; set; }
        public int LiteracyScore2 { get; set; }
        public int LiteracyScore3 { get; set; }
        public int LiteracyScore4 { get; set; }
        public string ModifiedBy { get; set; }
        public string DateModified { get; set; }
        public string CreatedBy { get; set; }
        public string DateCreated { get; set; }
    }
}