﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IT.ITTInterviews.Models
{
    public class PmInterviewModel
    {
        public int Id { get; set; }
        public int ApplicantId { get; set; }
        public string Subject { get; set; }
        public string Interviewer1 { get; set; }
        public string Interviewer2 { get; set; }
        public string DateInterview { get; set; }
        public int Question1Score { get; set; }
        public string Q1Notes { get; set; }
        public int Question2Score { get; set; }
        public string Q2Notes { get; set; }
        public int Question3Score { get; set; }
        public string Q3Notes { get; set; }
        public int Question4Score { get; set; }
        public string Q4Notes { get; set; }
        public int Question5Score { get; set; }
        public string Q5Notes { get; set; }
        public bool DbsCheckIssues { get; set; }
        public string DbsComment { get; set; }
        public bool IssuesStudying { get; set; }
        public string IssuesStudyingNotes { get; set; }
        public bool PhysicalHealth { get; set; }
        public string PhysicalHealthNotes { get; set; }
        public bool MentalHealth { get; set; }
        public string MentalHealthNotes { get; set; }
        public bool PersonalCircumstances { get; set; }
        public string PersonalCircumstancesNotes { get; set; }
        public bool ReligiousRequirements { get; set; }
        public string ReligiousRequirementsNotes { get; set; }
        public bool FailedQtsCourse { get; set; }
        public string FailedQtsCourseNotes { get; set; }
        public bool DbsFitnessIssues { get; set; }
        public string DbsFitnessIssuesNotes { get; set; }
        public string Questions { get; set; }
        public string Answers { get; set; }
        public string ModifiedBy { get; set; }
        public string ObservationDays { get; set; }
        public string CreatedBy { get; set; }        
    }
}