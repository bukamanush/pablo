﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IT.ITTInterviews.Models
{
    public class PresentationTaskModel
    {
        public int Id { get; set; }
        public int ApplicantId { get; set; }
        public string Observer { get; set; }
        public string Commentary { get; set; }
        public int Row1Score { get; set; }
        public int Row2Score { get; set; }
        public int Row3Score { get; set; }
        public int Row4Score { get; set; }
        public string ModifiedBy { get; set; }
        public string DateModified { get; set; }
        public string CreatedBy { get; set; }
        public string DateCreated { get; set; }

    }
}