﻿angular
    .module("StMarys.ctrl.pgcesecondaryteachersstandard", [])
    .controller("pgceSecondaryTeachersStandardController", [
        "$http", "$scope", "$location", "$compile", "$routeParams", "$modal", "$log", "modalproperties", "$modalStack", "$base64", "$linq", "$window", function ($http, $scope, $location, $compile, $routeParams, $modal, $log, modalproperties, $modalStack, $base64, $linq, $window) {

            $scope.data = {};
            $scope.item = {};
            $scope.applicantId = null;
            $scope.applicant1 = {};
            $scope.applicant2 = {};
            $scope.applicant3 = {};
            $scope.applicant4 = {};
            $scope.user = {
                username: $("#requestUserName").val()
            };
            $scope.applicants = [];
            $scope.showChooseApplicants = false;
            $scope.numApplicants = 0;
            $scope.showApplicants = false;
            $scope.list = [];
            $scope.candidates = [];
            $scope.mode = "create";
            
            $scope.order = function (predicate) {
                $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
                $scope.predicate = predicate;
            };

            $scope.initRequestor = function () {

                $http.get($("#wsbasehref").val() + "/itt/PgceSecondaryAdmin/FetchStaffById", { params: { username: $("#requestUserName").val() } })
                    .success(function (data) {
                        $scope.user.jobTitle = data.Title;
                        $scope.user.roomNumber = data.Branch;
                        $scope.user.departmentName = data.Department;
                        $scope.user.displayName = data.DisplayName;
                        $scope.loadApplicant();
                        if ($scope.mode === "create") {
                            $scope.initIncompleteApplicants();
                            $scope.resetItem();
                        } else {
                            $scope.loadStandards();
                        }
                    });

            };

            $scope.initIncompleteApplicants = function () {

                $http.get($("#wsbasehref").val() + "/itt/PgceSecondaryApplicants/FetchIncompleteTeachersStandards", { params: null })
                    .success(function (data) {
                        $scope.applicants = [];
                        for (var i = 0; i < data.length; i++) {
                            var l = new ApplicantModel(data[i]);
                            $scope.applicants.push(l);
                        }
                        $scope.showChooseApplicants = true;
                    });

            };

            $scope.addCandidate = function (applicant) {
               
                var c = new TsCandidateModel();
                c.applicant = applicant;
                c.id = $scope.numApplicants;
                c.applicantId = applicant.id;
                c.applicantName = applicant.displayName;
                $scope.candidates.push(c);
            };

            $scope.chooseApplicant = function (item) {
                $scope.numApplicants += 1;
                if ($scope.numApplicants <= 4) {
                    $scope.addCandidate(item);
                    $("#c" + item.id).addClass("list-group-item-success");
                    $scope.showApplicants = true;
                    if ($scope.numApplicants === 4) {
                        $scope.showChooseApplicants = false;
                    }
                } else {
                    $scope.showChooseApplicants = false;
                }
                
            }

            $scope.loadApplicant = function () {
                $http.get($("#wsbasehref").val() + "/itt/PgceSecondaryApplicants/FetchApplicant", { params: { id: $scope.applicantId } })
                     .success(function (data) {
                         var l = new ApplicantModel(data);
                         $scope.applicant = l;
                         
                     });


            };

            $scope.loadStandards = function () {
                $http.get($("#wsbasehref").val() + "/itt/PgceSecondaryApplicants/FetchTeachersStandard", { params: { id: $scope.applicantId } })
                     .success(function (data) {
                         var l = new TeachersStandardModel(data);
                         $scope.item = l;
                         $scope.item.observer = $scope.user.displayName;
                        $scope.fixList();
                         $modalStack.dismissAll("");
                         $(".page-body").show();
                     });


            };

            $scope.fixList = function () {
                $scope.candidates = [];
                if ($scope.item.applicant1Id > 0) {
                    var c1 = new TsCandidateModel();
                    c1.id = 1;
                    c1.applicantId = $scope.item.applicant1Id;
                    c1.applicantName = $scope.item.applicant1Name;
                    c1.applicantCommentary1 = $scope.item.applicant1Commentary1;
                    c1.applicantCommentary2 = $scope.item.applicant1Commentary2;
                    c1.applicantRow1 = $scope.item.applicant1Row1;
                    c1.applicantRow2 = $scope.item.applicant1Row2;
                    c1.applicantRow3 = $scope.item.applicant1Row3;
                    c1.applicantRow4 = $scope.item.applicant1Row4;
                    $scope.candidates.push(c1);
                }
                if ($scope.item.applicant2Id > 0) {
                    var c2 = new TsCandidateModel();
                    c2.id = 2;
                    c2.applicantId = $scope.item.applicant2Id;
                    c2.applicantName = $scope.item.applicant2Name;
                    c2.applicantCommentary1 = $scope.item.applicant2Commentary1;
                    c2.applicantCommentary2 = $scope.item.applicant2Commentary2;
                    c2.applicantRow1 = $scope.item.applicant2Row1;
                    c2.applicantRow2 = $scope.item.applicant2Row2;
                    c2.applicantRow3 = $scope.item.applicant2Row3;
                    c2.applicantRow4 = $scope.item.applicant2Row4;
                    $scope.candidates.push(c2);
                }
                if ($scope.item.applicant3Id > 0) {
                    var c3 = new TsCandidateModel();
                    c3.id = 3;
                    c3.applicantId = $scope.item.applicant3Id;
                    c3.applicantName = $scope.item.applicant3Name;
                    c3.applicantCommentary1 = $scope.item.applicant3Commentary1;
                    c3.applicantCommentary2 = $scope.item.applicant3Commentary2;
                    c3.applicantRow1 = $scope.item.applicant3Row1;
                    c3.applicantRow2 = $scope.item.applicant3Row2;
                    c3.applicantRow3 = $scope.item.applicant3Row3;
                    c3.applicantRow4 = $scope.item.applicant3Row4;
                    $scope.candidates.push(c3);
                }
                if ($scope.item.applicant4Id > 0) {
                    var c4 = new TsCandidateModel();
                    c4.id = 4;
                    c4.applicantId = $scope.item.applicant4Id;
                    c4.applicantName = $scope.item.applicant4Name;
                    c4.applicantCommentary1 = $scope.item.applicant4Commentary1;
                    c4.applicantCommentary2 = $scope.item.applicant4Commentary2;
                    c4.applicantRow1 = $scope.item.applicant4Row1;
                    c4.applicantRow2 = $scope.item.applicant4Row2;
                    c4.applicantRow3 = $scope.item.applicant4Row3;
                    c4.applicantRow4 = $scope.item.applicant4Row4;
                    $scope.candidates.push(c4);
                }
                $scope.numApplicants = $scope.candidates.length;
                $scope.showApplicants = true;
            };


            $scope.resetItem = function () {
                $scope.item = new TeachersStandardModel();
                $scope.item.createdBy = $scope.user.username;
                $scope.item.modifiedBy = $scope.user.username;
                $scope.item.observer = $scope.user.displayName;
                $modalStack.dismissAll("");
                $(".page-body").show();
            };

            $scope.fixItem = function() {
                if ($scope.candidates[0] !== undefined) {
                    $scope.item.applicant1Id = $scope.candidates[0].applicantId;
                    $scope.item.applicant1Name = $scope.candidates[0].applicantName;
                    $scope.item.applicant1Commentary1 = $scope.candidates[0].applicantCommentary1;
                    $scope.item.applicant1Commentary2 = $scope.candidates[0].applicantCommentary2;
                    $scope.item.applicant1Row1 = parseInt($scope.candidates[0].applicantRow1);
                    $scope.item.applicant1Row2 = parseInt($scope.candidates[0].applicantRow2);
                    $scope.item.applicant1Row3 = parseInt($scope.candidates[0].applicantRow3);
                    $scope.item.applicant1Row4 = parseInt($scope.candidates[0].applicantRow4);

                }
                if ($scope.candidates[1] !== undefined) {
                    $scope.item.applicant2Id = $scope.candidates[1].applicantId;
                    $scope.item.applicant2Name = $scope.candidates[1].applicantName;
                    $scope.item.applicant2Commentary1 = $scope.candidates[1].applicantCommentary1;
                    $scope.item.applicant2Commentary2 = $scope.candidates[1].applicantCommentary2;
                    $scope.item.applicant2Row1 = parseInt($scope.candidates[1].applicantRow1);
                    $scope.item.applicant2Row2 = parseInt($scope.candidates[1].applicantRow2);
                    $scope.item.applicant2Row3 = parseInt($scope.candidates[1].applicantRow3);
                    $scope.item.applicant2Row4 = parseInt($scope.candidates[1].applicantRow4);

                }
                if ($scope.candidates[2] !== undefined) {
                    $scope.item.applicant3Id = $scope.candidates[2].applicantId;
                    $scope.item.applicant3Name = $scope.candidates[2].applicantName;
                    $scope.item.applicant3Commentary1 = $scope.candidates[2].applicantCommentary1;
                    $scope.item.applicant3Commentary2 = $scope.candidates[2].applicantCommentary2;
                    $scope.item.applicant3Row1 = parseInt($scope.candidates[2].applicantRow1);
                    $scope.item.applicant3Row2 = parseInt($scope.candidates[2].applicantRow2);
                    $scope.item.applicant3Row3 = parseInt($scope.candidates[2].applicantRow3);
                    $scope.item.applicant3Row4 = parseInt($scope.candidates[2].applicantRow4);

                }
                if ($scope.candidates[3] !== undefined) {
                    $scope.item.applicant4Id = $scope.candidates[3].applicantId;
                    $scope.item.applicant4Name = $scope.candidates[3].applicantName;
                    $scope.item.applicant4Commentary1 = $scope.candidates[3].applicantCommentary1;
                    $scope.item.applicant4Commentary2 = $scope.candidates[3].applicantCommentary2;
                    $scope.item.applicant4Row1 = parseInt($scope.candidates[3].applicantRow1);
                    $scope.item.applicant4Row2 = parseInt($scope.candidates[3].applicantRow2);
                    $scope.item.applicant4Row3 = parseInt($scope.candidates[3].applicantRow3);
                    $scope.item.applicant4Row4 = parseInt($scope.candidates[3].applicantRow4);

                }

            };

            $scope.saveStandard = function () {

                var allAnswered = true;
                $("input:radio").each(function () {
                    var name = $(this).attr("name");
                    if ($("input:radio[name=" + name + "]:checked").length === 0) {
                        allAnswered = false;
                    }
                });

                if (allAnswered) {
                    $scope.fixItem();
                    $scope.bootOpen("lg", "Processing", "Please wait while your request is processing.");
                    var newitem = JSON.stringify($scope.item);
                    $http.post($("#wsbasehref").val() + "/itt/PgceSecondaryApplicants/SaveTeachersStandard",
                        {
                            newitem: newitem,
                            mode: $scope.mode
                        })
                        .success(function (data) {
                            document.location.href = $("#wsbasehref").val() + "/pgcesecondary/applications";
                        });
                } else {
                    $scope.open("lg", "Error", "Please answer each question");
                }
            };

            $scope.goTo = function (url) {
                document.location.href = $("#wsbasehref").val() + url;
            };

            $scope.goToHome = function () {
                document.location.href = "/";
            };

            $scope.load = function () {
                $scope.bootOpen("lg", "Processing", "Please wait while your request is processing.");
                var s = $location.path().split("/");
                if (s.length === 4) {
                    $scope.applicantId = parseInt(s[s.length - 1]);                    
                }
                $scope.mode = $.jqURL.get("m");
                $scope.initRequestor();
                
            };

            $scope.animationsEnabled = true;

            $scope.bootOpen = function (size, title, message) {
                var prop = { title: title, message: message };
                modalproperties.set(prop);
                var modalInstance = $modal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: "Modal.html",
                    controller: "ModalInstanceCtrl",
                    size: size
                });
                modalInstance.result.then(function () {
                }, function () {
                    $log.info("Modal dismissed at: " + new Date());
                });
            };

            $scope.open = function (size, title, message) {
                var prop = { title: title, message: message };
                modalproperties.set(prop);
                var modalInstance = $modal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: "ModalDialog.html",
                    controller: "ModalInstanceCtrl",
                    size: size
                });



                modalInstance.result.then(function () {

                }, function () {
                    $log.info("Modal dismissed at: " + new Date());
                });



            };

            $scope.toggleAnimation = function () {
                $scope.animationsEnabled = !$scope.animationsEnabled;
            };

            /* Open when someone clicks on the span element */
            $scope.openNav = function () {
                document.getElementById("myNav").style.width = "100%";
            };

            /* Close when someone clicks on the "x" symbol inside the overlay */
            $scope.closeNav = function () {
                document.getElementById("myNav").style.width = "0%";
            };

            $scope.load();



        }
    ])
.service("modalproperties", function () {
    var property = {
        title: "",
        message: ""
    };

    return {
        get: function () {
            return property;
        },
        set: function (value) {
            property = value;
        }
    };
}).filter("propsFilter", function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});

angular.module("StMarys.ctrl.pgcesecondaryteachersstandard").controller("ModalInstanceCtrl", ["$scope", "$modalInstance", "modalproperties", function ($scope, $modalInstance, modalproperties) {

    var prop = modalproperties.get();
    $scope.title = prop.title;
    $scope.message = prop.message;

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss("cancel");
    };
}]);

angular.module("StMarys.ctrl.pgcesecondaryteachersstandard")
.directive("randomBackgroundcolor", function () {
    return {
        restrict: "EA",
        replace: false,
        link: function (scope, element, attr) {

            var color = '#' + (Math.random() * 0xFFFFFF << 0).toString(16);
            element.css('border', '3px solid ' + color);
            element.css('border-color', color);
            element.css('border-radius', '12px');

        }
    }
});

