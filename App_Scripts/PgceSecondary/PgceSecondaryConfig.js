﻿angular
    .module("StMarys", [
    "ngRoute", "ui.bootstrap", "ngSanitize", "angucomplete-alt", "angular-linq", "ui.toggle", "base64", "ui.bootstrap.datetimepicker", "StMarys.ctrl.pgcesecondary", "StMarys.ctrl.pgcesecondaryappreview", "StMarys.ctrl.pgcesecondaryapplications", "StMarys.ctrl.pgcesecondaryliteracy", "StMarys.ctrl.pgcesecondarysktask", "StMarys.ctrl.pgcesecondarypresentation", "StMarys.ctrl.pgcesecondaryteachersstandard", "StMarys.ctrl.pgcesecondarypminterview", "StMarys.ctrl.pgcesecondarydecision"

    ])
    .config(["$routeProvider", "$locationProvider", "$httpProvider", function ($routeProvider, $locationProvider, $httpProvider) {

        $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=utf-8";
        var agentInfo = detect.parse(navigator.userAgent);
        if (agentInfo.browser.family === "IE") {
            if (!$httpProvider.defaults.headers.get) {
                $httpProvider.defaults.headers.get = {};
            }
            $httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
            $httpProvider.defaults.headers.get["If-Modified-Since"] = "0";
        }

        $routeProvider.when("/pgcesecondary", {
            templateUrl: "/PgceSecondary/StartForm",
            controller: "pgceSecondaryController"
        });

        $routeProvider.when("/pgcesecondary/applicationreview", {
            templateUrl: "/PgceSecondary/ApplicationReviewForm",
            controller: "pgceSecondaryAppReviewController"
        });

        $routeProvider.when("/pgcesecondary/applications", {
            templateUrl: "/PgceSecondary/ApplicationsForm",
            controller: "pgceSecondaryApplicationsController"
        });


        $routeProvider.when("/pgcesecondary/applications/:id", {
            templateUrl: "/PgceSecondary/ApplicationsForm",
            controller: "pgceSecondaryApplicationsController"
        });

        $routeProvider.when("/pgcesecondary/teachersstandard", {
            templateUrl: "/PgceSecondary/TeachersStandardForm",
            controller: "pgceSecondaryTeachersStandardController"
        });

        $routeProvider.when("/pgcesecondary/teachersstandard/:id", {
            templateUrl: "/PgceSecondary/TeachersStandardForm",
            controller: "pgceSecondaryTeachersStandardController"
        });

        $routeProvider.when("/pgcesecondary/literacy/:id", {
            templateUrl: "/PgceSecondary/LiteracyForm",
            controller: "pgceSecondaryLiteracyController"
        });

        //SKTask
        $routeProvider.when("/pgcesecondary/sktask/:id", {
            templateUrl: "/PgceSecondary/SKTaskForm",
            controller: "pgceSecondarySKTaskController" //pgceSecondarySKTaskController
        });

        $routeProvider.when("/pgcesecondary/presentation/:id", {
            templateUrl: "/PgceSecondary/PresentationForm",
            controller: "pgceSecondaryPresentationController"
        });

        $routeProvider.when("/pgcesecondary/pminterview/:id", {
            templateUrl: "/PgceSecondary/PmInterviewForm",
            controller: "pgceSecondaryPmInterviewController"
        });

        $routeProvider.when("/pgcesecondary/decision/:id", {
            templateUrl: "/PgceSecondary/DecisionForm",
            controller: "pgceSecondaryDecisionController"
        });

        $routeProvider.when("/pgcesecondary/decisionone/:id", {
            templateUrl: "/PgceSecondary/DecisionOneForm",
            controller: "pgceSecondaryDecisionController"
        });


        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });



        /**
   * The workhorse; converts an object to x-www-form-urlencoded serialization.
   * @param {Object} obj
   * @return {String}
   */
        var param = function (obj) {
            var query = "", name, value, fullSubName, subName, subValue, innerObj, i;

            for (name in obj) {
                value = obj[name];

                if (value instanceof Array) {
                    for (i = 0; i < value.length; ++i) {
                        subValue = value[i];
                        fullSubName = name + "[" + i + "]";
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + "&";
                    }
                }
                else if (value instanceof Object) {
                    for (subName in value) {
                        subValue = value[subName];
                        fullSubName = name + "[" + subName + "]";
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + "&";
                    }
                }
                else if (value !== undefined && value !== null)
                    query += encodeURIComponent(name) + "=" + encodeURIComponent(value) + "&";
            }

            return query.length ? query.substr(0, query.length - 1) : query;

        };


        $httpProvider.defaults.transformRequest = [
            function (data) {
                return angular.isObject(data) && String(data) !== "[object File]" ? param(data) : data;
            }
        ];


    }]);