﻿angular
    .module("StMarys.ctrl.pgcesecondaryappreview", [])
    .controller("pgceSecondaryAppReviewController", [
        "$http", "$scope", "$location", "$compile", "$routeParams", "$modal", "$log", "modalproperties", "$modalStack", "$base64", "$linq", "$window", function ($http, $scope, $location, $compile, $routeParams, $modal, $log, modalproperties, $modalStack, $base64, $linq, $window) {

            $scope.data = {};
            $scope.item = {};
            $scope.user = {
                username: $("#requestUserName").val()
            };
            $scope.showListings = false;
            $scope.showAdd = false;
            $scope.showEdit = false;
            $scope.list = [];
            $scope.types = [];
            $scope.countries = [];
            $scope.templates = [];

            $scope.order = function (predicate) {
                $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
                $scope.predicate = predicate;
            };

            $scope.initRequestor = function () {

                $http.get($("#wsbasehref").val() + "/itt/PgceSecondaryAdmin/FetchStaffById", { params: { username: $("#requestUserName").val() } })
                    .success(function (data) {
                        $scope.user.jobTitle = data.Title;
                        $scope.user.roomNumber = data.Branch;
                        $scope.user.departmentName = data.Department;
                        $scope.resetItem();
                        $modalStack.dismissAll("");
                        $(".page-body").show();
                        
                    });

            };

            $scope.resetItem = function() {
                $scope.item = new ApplicantModel();
                $scope.item.createdBy = $scope.user.username;
                $scope.item.modifiedBy = $scope.user.username;
            };


            $scope.saveNewApplicationReview = function() {
                $scope.bootOpen("lg", "Processing", "Please wait while your request is processing.");
                $scope.item.decision = ($scope.item.decision == 'true' ? true : false); //conversion to bool.
                var newitem = JSON.stringify($scope.item);
                
                $http.post($("#wsbasehref").val() + "/itt/PgceSecondaryApplicants/AddNewApplicationReview", {
                    newitem: newitem
                }).success(function (data) {
                    document.location.href = $("#wsbasehref").val() + "/pgcesecondary";
                });
            };

            $scope.goTo = function (url) {
                document.location.href = $("#wsbasehref").val() + url;
            };


            $scope.goToHome = function () {
                document.location.href = "/";
            };

            $scope.load = function () {
                $scope.bootOpen("lg", "Processing", "Please wait while your request is processing.");
                $scope.initRequestor();

            };

            $scope.animationsEnabled = true;

            $scope.bootOpen = function (size, title, message) {
                var prop = { title: title, message: message };
                modalproperties.set(prop);
                var modalInstance = $modal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: "Modal.html",
                    controller: "ModalInstanceCtrl",
                    size: size
                });
                modalInstance.result.then(function () {
                }, function () {
                    $log.info("Modal dismissed at: " + new Date());
                });
            };

            $scope.open = function (size, title, message) {
                var prop = { title: title, message: message };
                modalproperties.set(prop);
                var modalInstance = $modal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: "ModalDialog.html",
                    controller: "ModalInstanceCtrl",
                    size: size
                });



                modalInstance.result.then(function () {

                }, function () {
                    $log.info("Modal dismissed at: " + new Date());
                });



            };

            $scope.toggleAnimation = function () {
                $scope.animationsEnabled = !$scope.animationsEnabled;
            };

            /* Open when someone clicks on the span element */
            $scope.openNav = function () {
                document.getElementById("myNav").style.width = "100%";
            };

            /* Close when someone clicks on the "x" symbol inside the overlay */
            $scope.closeNav = function () {
                document.getElementById("myNav").style.width = "0%";
            };

            $scope.load();



        }
    ])
.service("modalproperties", function () {
    var property = {
        title: "",
        message: ""
    };

    return {
        get: function () {
            return property;
        },
        set: function (value) {
            property = value;
        }
    };
}).filter("propsFilter", function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});

angular.module("StMarys.ctrl.pgcesecondaryappreview").controller("ModalInstanceCtrl", ["$scope", "$modalInstance", "modalproperties", function ($scope, $modalInstance, modalproperties) {

    var prop = modalproperties.get();
    $scope.title = prop.title;
    $scope.message = prop.message;

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss("cancel");
    };
}]);

angular.module("StMarys.ctrl.pgcesecondaryappreview")
.directive("randomBackgroundcolor", function () {
    return {
        restrict: "EA",
        replace: false,
        link: function (scope, element, attr) {

            var color = '#' + (Math.random() * 0xFFFFFF << 0).toString(16);
            element.css('border', '3px solid ' + color);
            element.css('border-color', color);
            element.css('border-radius', '12px');

        }
    }
});

