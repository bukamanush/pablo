﻿angular
    .module("StMarys", [
    "ngRoute", "ui.bootstrap", "ngSanitize", "angucomplete-alt", "angular-linq", "ui.toggle", "base64", "ui.bootstrap.datetimepicker", "StMarys.ctrl.home"

    ])
    .config(["$routeProvider", "$locationProvider", "$httpProvider", function ($routeProvider, $locationProvider, $httpProvider) {

        $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=utf-8";
        var agentInfo = detect.parse(navigator.userAgent);
        if (agentInfo.browser.family === "IE") {
            if (!$httpProvider.defaults.headers.get) {
                $httpProvider.defaults.headers.get = {};
            }
            $httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
            $httpProvider.defaults.headers.get["If-Modified-Since"] = "0";
        }

        $routeProvider.when("/", {
            templateUrl: "/Home/HomeForm",
            controller: "homeController"
        });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });



        /**
   * The workhorse; converts an object to x-www-form-urlencoded serialization.
   * @param {Object} obj
   * @return {String}
   */
        var param = function (obj) {
            var query = "", name, value, fullSubName, subName, subValue, innerObj, i;

            for (name in obj) {
                value = obj[name];

                if (value instanceof Array) {
                    for (i = 0; i < value.length; ++i) {
                        subValue = value[i];
                        fullSubName = name + "[" + i + "]";
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + "&";
                    }
                }
                else if (value instanceof Object) {
                    for (subName in value) {
                        subValue = value[subName];
                        fullSubName = name + "[" + subName + "]";
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + "&";
                    }
                }
                else if (value !== undefined && value !== null)
                    query += encodeURIComponent(name) + "=" + encodeURIComponent(value) + "&";
            }

            return query.length ? query.substr(0, query.length - 1) : query;

        };


        $httpProvider.defaults.transformRequest = [
            function (data) {
                return angular.isObject(data) && String(data) !== "[object File]" ? param(data) : data;
            }
        ];


    }]);