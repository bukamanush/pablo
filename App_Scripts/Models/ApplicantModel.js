﻿var ApplicantModel = function (data) {
    var self = this;
    data = data || {};

    self.id = data.Id;
    self.forename = data.Forname;
    self.surname = data.Surname;
    self.subject = data.Subject;
    self.displayName = data.DisplayName;
    self.ucasId = data.UcasId;
    self.teachersStandard = data.TeachersStandard;
    self.literacy = data.Literacy;
    self.sKTask = data.SKTask;
    self.presentation = data.Presentation;
    self.interviewPm = data.InterviewPm;
    self.decision = data.Decision;
    self.modifiedBy = data.ModifiedBy;
    self.createdBy = data.CreatedBy;
    //added
    self.rejectReason = data.RejectReason;
    self.interviewReject = data.InterviewReject;
    self.rejectHighlight = data.RejectHighlight;
    self.reasonToInviteInterview = data.ReasonToInviteInterview;
}