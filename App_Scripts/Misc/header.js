﻿angular.module("StMarys")
    .directive("fullscreen", function () {
        return {
            restrict: "AC",
            template: "<i class=\"glyphicon glyphicon-fullscreen\"></i>",
            link: function (scope, el, attr) {
                el.on("click", function () {
                    var element = document.documentElement;
                    if (!$("body")
                        .hasClass("full-screen")) {

                        $("body")
                            .addClass("full-screen");
                        $("#fullscreen-toggler")
                            .addClass("active");
                        if (element.requestFullscreen) {
                            element.requestFullscreen();
                        } else if (element.mozRequestFullScreen) {
                            element.mozRequestFullScreen();
                        } else if (element.webkitRequestFullscreen) {
                            element.webkitRequestFullscreen();
                        } else if (element.msRequestFullscreen) {
                            /*element.msRequestFullscreen();*/
                        }

                    } else {

                        $("body").removeClass("full-screen");
                        el.removeClass("active");

                        if (document.exitFullscreen) {
                            document.exitFullscreen();
                        } else if (document.mozCancelFullScreen) {
                            document.mozCancelFullScreen();
                        } else if (document.webkitExitFullscreen) {
                            document.webkitExitFullscreen();
                        }

                    }
                });
            }
        };
    }
    );

angular.module("StMarys")
    .directive("sidebarToggler", function () {
        return {
            restrict: "AC",
            template: "<i class=\"fa fa-arrows-h\"></i>",
            link: function (scope, el, attr) {
                el.on("click", function () {
                    $("#sidebar").toggleClass("hide");
                    el.toggleClass("active");
                    return false;
                });
            }
        };
    }
    );

angular.module("StMarys")
    .directive("pageTitle", [
        "$rootScope", "$timeout",
        function ($rootScope, $timeout) {
            return {
                link: function (scope, element) {

                    var listener = function (event, toState) {
                        var title = "Default Title";
                        if (toState.ncyBreadcrumb && toState.ncyBreadcrumb.label) title = toState.ncyBreadcrumb.label;
                        $timeout(function () {
                            element.text(title);
                        }, 0, false);
                    };
                    $rootScope.$on("$stateChangeSuccess", listener);
                }
            };
        }
    ]);

angular.module("StMarys")
    .directive("headerTitle", [
        "$rootScope", "$timeout",
        function ($rootScope, $timeout) {
            return {
                link: function (scope, element) {

                    var listener = function (event, toState) {
                        var title = "Default Title";
                        var description = "";
                        if (toState.ncyBreadcrumb && toState.ncyBreadcrumb.label) title = toState.ncyBreadcrumb.label;
                        if (toState.ncyBreadcrumb && toState.ncyBreadcrumb.description) description = toState.ncyBreadcrumb.description;
                        $timeout(function () {
                            if (description == "")
                                element.text(title);
                            else
                                element.html(title + " <small> <i class=\"fa fa-angle-right\"> </i> " + description + " </small>");
                        }, 0, false);
                    };
                    $rootScope.$on("$stateChangeSuccess", listener);
                }
            };
        }
    ]);

var editingForm = false;

function GoTo(page) {
    if (!editingForm) {
        document.location.href = page;
    } else {
       if (confirm("Are you sure you wish to leave the page without saving?")) {
           document.location.href = page;
       }
    }
}

//Sidebar Menu Handle
angular.module("StMarys")
    .directive("sidebarMenu", function () {
        return {
            restrict: "AC",
            link: function (scope, el, attr) {
                el.find("li.active").parents("li").addClass("active open");

                el.on("click", "a", function (e) {
                        e.preventDefault();
                        var isCompact = $("#sidebar").hasClass("menu-compact");
                        var menuLink = $(e.target);
                        if ($(e.target).is("span"))
                            menuLink = $(e.target).closest("a");
                        if (!menuLink || menuLink.length == 0)
                            return;
                        if (!menuLink.hasClass("menu-dropdown")) {
                            if (isCompact && menuLink.get(0).parentNode.parentNode == this) {
                                var menuText = menuLink.find(".menu-text").get(0);
                                if (e.target != menuText && !$.contains(menuText, e.target)) {
                                    return false;
                                }
                            }
                            return;
                        }
                        var submenu = menuLink.next().get(0);
                        if (!$(submenu).is(":visible")) {
                            var c = $(submenu.parentNode).closest("ul");
                            if (isCompact && c.hasClass("sidebar-menu"))
                                return;
                            c.find("* > .open > .submenu")
                                .each(function() {
                                    if (this != submenu && !$(this.parentNode).hasClass("active"))
                                        $(this).slideUp(200).parent().removeClass("open");
                                });
                        }
                        if (isCompact && $(submenu.parentNode.parentNode).hasClass("sidebar-menu"))
                            return false;
                        $(submenu).slideToggle(200).parent().toggleClass("open");
                        return false;
                    
                });
            }
        };
    });

//Sidebar Collapse
angular.module("StMarys")
    .directive("sidebarCollapse", function () {
        return {
            restrict: "AC",
            template: '<i class="collapse-icon fa fa-bars"></i>',
            link: function (scope, el, attr) {
                el.on("click", function () {
                    if (!$("#sidebar").is(":visible"))
                        $("#sidebar").toggleClass("hide");
                    $("#sidebar").toggleClass("menu-compact");
                    $(".sidebar-collapse").toggleClass("active");
                    var isCompact = $("#sidebar").hasClass("menu-compact");

                    if ($(".sidebar-menu").closest("div").hasClass("slimScrollDiv")) {
                        $(".sidebar-menu").slimScroll({ destroy: true });
                        $(".sidebar-menu").attr("style", "");
                    }
                    if (isCompact) {
                        $(".open > .submenu")
                            .removeClass("open");
                    } else {
                        if ($(".page-sidebar").hasClass("sidebar-fixed")) {
                            var position = (readCookie("rtl-support") || location.pathname == "/index-rtl-fa.html" || location.pathname == "/index-rtl-ar.html") ? "right" : "left";
                            $(".sidebar-menu").slimscroll({
                                height: $(window).height() - 90,
                                position: position,
                                size: "3px",
                                color: themeprimary
                            });
                        }
                    }
                    //Slim Scroll Handle
                });
            }
        };
    });

