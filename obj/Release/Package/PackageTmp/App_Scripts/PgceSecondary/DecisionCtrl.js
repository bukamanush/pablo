﻿angular
    .module("StMarys.ctrl.pgcesecondarydecision", [])
    .controller("pgceSecondaryDecisionController", [
        "$http", "$scope", "$location", "$compile", "$routeParams", "$modal", "$log", "modalproperties", "$modalStack", "$base64", "$linq", "$window", function ($http, $scope, $location, $compile, $routeParams, $modal, $log, modalproperties, $modalStack, $base64, $linq, $window) {

            $scope.data = {};
            $scope.item = {};
            $scope.applicantId = null;
            $scope.applicant = {};
            $scope.user = {
                username: $("#requestUserName").val()
            };
            $scope.showListings = false;
            $scope.showAdd = false;
            $scope.showOtherReason = false;
            $scope.list = [];
            $scope.types = [];
            $scope.countries = [];
            $scope.templates = [];
            $scope.mode = "create";
            $scope.otherReason = "";
            $scope.reasons = [
                { id:0, name: "Presentation", selected: false },
                { id:1, name: "Discussion Activity", selected: false },
                { id:2, name: "Subject Knowledge Assessment", selected: false },
                { id:3, name: "English Writing Task", selected: false },
                { id:4, name: "Afternoon Interview", selected: false },
                { id:5, name: "Other", selected: false }
            
            ];

            $scope.order = function (predicate) {
                $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
                $scope.predicate = predicate;
            };

            $scope.initRequestor = function () {

                $http.get($("#wsbasehref").val() + "/itt/PgceSecondaryAdmin/FetchStaffById", { params: { username: $("#requestUserName").val() } })
                    .success(function (data) {
                        $scope.user.jobTitle = data.Title;
                        $scope.user.roomNumber = data.Branch;
                        $scope.user.departmentName = data.Department;
                        $scope.user.displayName = data.DisplayName;
                        $scope.loadApplicant();
                        if ($scope.mode === "create") {
                            $scope.resetItem();
                        } else {
                            $scope.loadDecision();
                        }
                    });

            };

            $scope.loadApplicant = function () {
                $http.get($("#wsbasehref").val() + "/itt/PgceSecondaryApplicants/FetchApplicant", { params: { id: $scope.applicantId } })
                     .success(function (data) {
                         var l = new ApplicantModel(data);
                         $scope.applicant = l;
                         $modalStack.dismissAll("");
                         $(".page-body").show();
                     });


            };

            $scope.loadDecision = function () {
                $http.get($("#wsbasehref").val() + "/itt/PgceSecondaryApplicants/FetchDecision", { params: { id: $scope.applicantId } })
                     .success(function (data) {
                         var l = new DecisionModel(data);
                         $scope.item = l;
                        $scope.fixCheckboxDecisions();
                    });
            };

            $scope.fixCheckboxDecisions = function () {
                for (var i = 0; i < $scope.reasons.length; i++) {
                   if ($scope.item.rejectionMain.indexOf($scope.reasons[i].name) > -1) {
                        $scope.reasons[i].selected = true;
                    }                   
                }
            };

            $scope.resetItem = function () {
                $scope.item = new DecisionModel();
                $scope.item.applicantId = $scope.applicantId;
                $scope.item.createdBy = $scope.user.username;
                $scope.item.modifiedBy = $scope.user.username;
                

            };

            $scope.changedDecision = function(answer) {
                if (answer === "accept") {
                    if ($scope.item.acceptedConditionally) {
                        $scope.item.rejected = false;
                    }   
                }
                if (answer === "reject") {
                    if ($scope.item.rejected) {
                        $scope.item.acceptedConditionally = false;
                    }
                }
            };

            $scope.changedReason = function (reason) {
               if (reason.id === 5) {
                    $scope.showOtherReason = true;
                }
            };

            $scope.saveDecision = function () {
                var reasons = $linq.Enumerable()
                    .From($scope.reasons)
                    .Where(function(x) { return x.selected === true; })
                    .Select(function (x) { return x.name; }).ToArray();
                if (reasons.length>0) {
                    $scope.item.rejectionMain = reasons.join(",") + " " + $scope.otherReason;
                }

                    $scope.bootOpen("lg", "Processing", "Please wait while your request is processing.");
                    var newitem = JSON.stringify($scope.item);
                    $http.post($("#wsbasehref").val() + "/itt/PgceSecondaryApplicants/SaveDecision",
                        {
                            newitem: newitem,
                            mode: $scope.mode
                        })
                        .success(function (data) {
                            document.location.href = $("#wsbasehref").val() +
                                "/pgcesecondary/applications/" + data.Message;
                        });
               
            };

            $scope.goTo = function (url) {
                document.location.href = $("#wsbasehref").val() + url;
            };

            $scope.goToHome = function () {
                document.location.href = "/";
            };

            $scope.load = function () {
                $scope.bootOpen("lg", "Processing", "Please wait while your request is processing.");
                var s = $location.path().split("/");
                if (s.length === 4) {
                    $scope.applicantId = parseInt(s[s.length - 1]);
                    $scope.mode = $.jqURL.get("m");
                }
                $scope.initRequestor();
               
                  
            };

            $scope.animationsEnabled = true;

            $scope.bootOpen = function (size, title, message) {
                var prop = { title: title, message: message };
                modalproperties.set(prop);
                var modalInstance = $modal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: "Modal.html",
                    controller: "ModalInstanceCtrl",
                    size: size
                });
                modalInstance.result.then(function () {
                }, function () {
                    $log.info("Modal dismissed at: " + new Date());
                });
            };

            $scope.open = function (size, title, message) {
                var prop = { title: title, message: message };
                modalproperties.set(prop);
                var modalInstance = $modal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: "ModalDialog.html",
                    controller: "ModalInstanceCtrl",
                    size: size
                });



                modalInstance.result.then(function () {

                }, function () {
                    $log.info("Modal dismissed at: " + new Date());
                });



            };

            $scope.toggleAnimation = function () {
                $scope.animationsEnabled = !$scope.animationsEnabled;
            };

            /* Open when someone clicks on the span element */
            $scope.openNav = function () {
                document.getElementById("myNav").style.width = "100%";
            };

            /* Close when someone clicks on the "x" symbol inside the overlay */
            $scope.closeNav = function () {
                document.getElementById("myNav").style.width = "0%";
            };

            $scope.load();



        }
    ])
.service("modalproperties", function () {
    var property = {
        title: "",
        message: ""
    };

    return {
        get: function () {
            return property;
        },
        set: function (value) {
            property = value;
        }
    };
}).filter("propsFilter", function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});

angular.module("StMarys.ctrl.pgcesecondarydecision").controller("ModalInstanceCtrl", ["$scope", "$modalInstance", "modalproperties", function ($scope, $modalInstance, modalproperties) {

    var prop = modalproperties.get();
    $scope.title = prop.title;
    $scope.message = prop.message;

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss("cancel");
    };
}]);

angular.module("StMarys.ctrl.pgcesecondarydecision")
.directive("randomBackgroundcolor", function () {
    return {
        restrict: "EA",
        replace: false,
        link: function (scope, element, attr) {

            var color = '#' + (Math.random() * 0xFFFFFF << 0).toString(16);
            element.css('border', '3px solid ' + color);
            element.css('border-color', color);
            element.css('border-radius', '12px');

        }
    }
});

