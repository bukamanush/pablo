﻿angular
    .module("StMarys.ctrl.pgcesecondaryapplications", [])
    .controller("pgceSecondaryApplicationsController", [
        "$http", "$scope", "$location", "$compile", "$routeParams", "$modal", "$log", "modalproperties", "$modalStack", "$base64", "$linq", "$window", function ($http, $scope, $location, $compile, $routeParams, $modal, $log, modalproperties, $modalStack, $base64, $linq, $window) {

            $scope.data = {};
            $scope.item = {};
            $scope.applicant = {};
            $scope.applicantId = null;
            $scope.selectedStudents = [];
            $scope.user = {
                username: $("#requestUserName").val()
            };
            $scope.showListings = false;
            $scope.showStandard = false;
            $scope.showLiteracy = false;
            $scope.showPresentation = false;
            $scope.showPmInterview = false;
            $scope.showDecision = false;
            $scope.list = [];
            $scope.types = [];
            $scope.countries = [];
            $scope.templates = [];
            $scope.literacy = {};
            $scope.presentation = {};
            $scope.candidate = {};
            $scope.interview = {};
            $scope.decision = {};
            $scope.adminUser = false;
            $scope.adminusers = ["harrisoh", "03931", "00583", "05731", "51542i"];
            $scope.status = null;

            $scope.order = function (predicate) {
                $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
                $scope.predicate = predicate;
            };

            $scope.initRequestor = function () {

                $http.get($("#wsbasehref").val() + "/itt/PgceSecondaryAdmin/FetchStaffById", { params: { username: $("#requestUserName").val() } })
                    .success(function (data) {
                        $scope.user.jobTitle = data.Title;
                        $scope.user.roomNumber = data.Branch;
                        $scope.user.departmentName = data.Department;
                        if ($scope.adminusers.indexOf($scope.user.username) > -1) {
                            $scope.adminUser = true;
                        }
                        if ($.jqURL.get("status") === "1") {
                            $scope.status = 1;
                        }
                       
                        if ($scope.applicantId === null) {
                            $scope.loadList();
                        } else {
                            $scope.loadItem();
                        }

                    });

            };

            $scope.selectAll = function (value) {

                $scope.selectedStudents.length = 0;
              
                if (!value) { //checkbox got unchecked.
                    return;
                }

                for (var i = 0; i < $scope.list.length ; i++) {
                    $scope.selectedStudents.push($scope.list[i].id);
                }

            }

            $scope.selectStudent = function (value, id) {
                if (value) { //checkbox checked
                    if ($scope.selectedStudents.indexOf(id) == -1) {
                        $scope.selectedStudents.push(id);
                    }
                } else { //checkbox unchecked
                    if ($scope.selectedStudents.indexOf(id) !== -1) { //make sure its in the array
                        $scope.selectedStudents.splice($scope.selectedStudents.indexOf(id), 1);
                    }
                   
                }
               
            }

            $scope.exportData = function () {
                $http.post($("#wsbasehref").val() + "/itt/PgceSecondaryApplicants/exportData", 
                { applicantIds: $scope.selectedStudents.map(function(id){
                return parseInt(id,10)}
                )})
                    .success(function (data) {
                        var response = JSON.parse(data);
                       /* window.location = '/Report/Download?fileGuid=' + response.FileGuid
                                          + '&filename=' + response.FileName;*/
                        //alert('it works');
                    });
            }

            $scope.toggleCheck = function (list) {
                if ($scope.loadList.indexOf(list) === -1) {
                    $scope.loadList.push(list);
                } else {
                    $scope.loadList.splice($scope.loadList.indexOf(list), 1);
                }
            }

            $scope.loadList = function () {
            $http.get($("#wsbasehref").val() + "/itt/PgceSecondaryApplicants/FetchApplicants", { params: {status: $scope.status} })
                    .success(function (data) {
                    for (var i = 0; i < data.length; i++) {
                        var l = new ApplicantModel(data[i]);
                        $scope.list.push(l);
                    }
                    $modalStack.dismissAll("");
                    $(".page-body").show();
                    
                    });
            };

            $scope.archiveApplicant = function (id) {
                $http.get($("#wsbasehref").val() + "/itt/PgceSecondaryApplicants/ArchiveApplicant", { params: { id: id } })
                     .success(function (data) {
                        document.location.href = $("#wsbasehref").val() + "/pgcesecondary/applications";
                    });
            };

            $scope.removeApplicant = function (id) {
                if (confirm("Are you sure you wish to delete this applicant?")) {
                    $http.get($("#wsbasehref").val() + "/itt/PgceSecondaryApplicants/RemoveApplicant",
                            { params: { id: id } })
                        .success(function(data) {
                            document.location.href = $("#wsbasehref").val() + "/pgcesecondary/applications";
                        });
                }
            };

            $scope.loadItem = function () {
                $http.get($("#wsbasehref").val() + "/itt/PgceSecondaryApplicants/FetchApplicant", { params: {id: $scope.applicantId } })
                     .success(function (data) {
                         $scope.list = [];
                         var l = new ApplicantModel(data);
                         $scope.applicant = l;
                         $scope.list.push(l);
                         $modalStack.dismissAll("");
                         $(".page-body").show();
                         $scope.loadTeachersStandard();
                         $scope.loadLiteracy();
                         $scope.loadPresentation();
                         $scope.loadPmInterview();
                        $scope.loadDecision();

                    });


            };

            $scope.fixStandard = function () {
                $scope.candidate = {};
                if ($scope.item.applicant1Id === $scope.applicantId) {
                    var c1 = new TsCandidateModel();
                    c1.id = 1;
                    c1.applicantId = $scope.item.applicant1Id;
                    c1.applicantName = $scope.item.applicant1Name;
                    c1.applicantCommentary1 = $scope.item.applicant1Commentary1;
                    c1.applicantCommentary2 = $scope.item.applicant1Commentary2;
                    c1.applicantRow1 = $scope.item.applicant1Row1;
                    c1.applicantRow2 = $scope.item.applicant1Row2;
                    c1.applicantRow3 = $scope.item.applicant1Row3;
                    c1.applicantRow4 = $scope.item.applicant1Row4;
                    $scope.candidate = c1;
                }
                if ($scope.item.applicant2Id === $scope.applicantId) {
                    var c2 = new TsCandidateModel();
                    c2.id = 2;
                    c2.applicantId = $scope.item.applicant2Id;
                    c2.applicantName = $scope.item.applicant2Name;
                    c2.applicantCommentary1 = $scope.item.applicant2Commentary1;
                    c2.applicantCommentary2 = $scope.item.applicant2Commentary2;
                    c2.applicantRow1 = $scope.item.applicant2Row1;
                    c2.applicantRow2 = $scope.item.applicant2Row2;
                    c2.applicantRow3 = $scope.item.applicant2Row3;
                    c2.applicantRow4 = $scope.item.applicant2Row4;
                    $scope.candidate = c2;
                }
                if ($scope.item.applicant3Id === $scope.applicantId) {
                    var c3 = new TsCandidateModel();
                    c3.id = 3;
                    c3.applicantId = $scope.item.applicant3Id;
                    c3.applicantName = $scope.item.applicant3Name;
                    c3.applicantCommentary1 = $scope.item.applicant3Commentary1;
                    c3.applicantCommentary2 = $scope.item.applicant3Commentary2;
                    c3.applicantRow1 = $scope.item.applicant3Row1;
                    c3.applicantRow2 = $scope.item.applicant3Row2;
                    c3.applicantRow3 = $scope.item.applicant3Row3;
                    c3.applicantRow4 = $scope.item.applicant3Row4;
                    $scope.candidate = c3;
                }
                if ($scope.item.applicant4Id === $scope.applicantId) {
                    var c4 = new TsCandidateModel();
                    c4.id = 4;
                    c4.applicantId = $scope.item.applicant4Id;
                    c4.applicantName = $scope.item.applicant4Name;
                    c4.applicantCommentary1 = $scope.item.applicant4Commentary1;
                    c4.applicantCommentary2 = $scope.item.applicant4Commentary2;
                    c4.applicantRow1 = $scope.item.applicant4Row1;
                    c4.applicantRow2 = $scope.item.applicant4Row2;
                    c4.applicantRow3 = $scope.item.applicant4Row3;
                    c4.applicantRow4 = $scope.item.applicant4Row4;
                    $scope.candidate = c4;
                }
                
            };
            
            $scope.loadTeachersStandard = function() {
                $http.get($("#wsbasehref").val() + "/itt/PgceSecondaryApplicants/FetchTeachersStandard", { params: { id: $scope.applicantId } })
                     .success(function (data) {
                         $scope.item = new TeachersStandardModel(data);
                         $scope.fixStandard();
                        $scope.showStandard = true;
                    });
            };

            $scope.loadLiteracy = function () {
                $http.get($("#wsbasehref").val() + "/itt/PgceSecondaryApplicants/FetchLiteracyTask", { params: { id: $scope.applicantId } })
                     .success(function (data) {
                         $scope.literacy = new LiteracyTaskModel(data);
                         $scope.showLiteracy = true;
                     });
            };

            $scope.loadPresentation = function () {
                $http.get($("#wsbasehref").val() + "/itt/PgceSecondaryApplicants/FetchPresentationTask", { params: { id: $scope.applicantId } })
                     .success(function (data) {
                         $scope.presentation = new PresentationTaskModel(data);
                         $scope.showPresentation = true;
                     });
            };

            $scope.loadPmInterview = function () {
                $http.get($("#wsbasehref").val() + "/itt/PgceSecondaryApplicants/FetchPmInterview", { params: { id: $scope.applicantId } })
                     .success(function (data) {
                         $scope.interview = new PmInterviewModel(data);
                         $scope.showPmInterview = true;
                     });
            };

            $scope.loadDecision = function () {
                $http.get($("#wsbasehref").val() + "/itt/PgceSecondaryApplicants/FetchDecision", { params: { id: $scope.applicantId } })
                     .success(function (data) {
                         $scope.decision = new DecisionModel(data);
                         $scope.showDecision = true;
                     });
            };

            $scope.goTo = function (url) {
                document.location.href = $("#wsbasehref").val() + url;
            };

            $scope.goCreate = function (item, task) {
               var url = $("#wsbasehref").val();
                switch (task) {
                case "literacy":
                    url += "/pgcesecondary/literacy/" + item.id + "?m=create";
                    break;
                case "sktask":
                    url += "/pgcesecondary/sktask/" + item.id + "?m=create";
                    break;
                case "presentation":
                    url += "/pgcesecondary/presentation/" + item.id + "?m=create";
                    break;
                    case "teachersstandard":
                        url += "/pgcesecondary/teachersstandard?m=create";
                        break;
                    case "pminterview":
                        url += "/pgcesecondary/pminterview/" + item.id + "?m=create";
                        break;
                    case "decision":
                        url += "/pgcesecondary/decision/" + item.id + "?m=create";
                        break;
                default:
                }

                document.location.href = url;
            };

            $scope.goEdit = function (item, task) {
               var url = $("#wsbasehref").val();
                switch (task) {
                    case "literacy":
                        url += "/pgcesecondary/literacy/" + item.id + "?m=update";
                        break;
                    case "sktask":
                        url += "/pgcesecondary/sktask/" + item.id + "?m=update";
                        break;
                    case "presentation":
                        url += "/pgcesecondary/presentation/" + item.id + "?m=update";
                        break;
                    case "teachersstandard":
                        url += "/pgcesecondary/teachersstandard/" + item.id + "?m=update";
                        break;
                    case "summary":
                        url += "/pgcesecondary/applications/" + item.id;
                        break;
                    case "pminterview":
                        url += "/pgcesecondary/pminterview/" + item.id + "?m=update";
                        break;
                    case "decision":
                        url += "/pgcesecondary/decision/" + item.id + "?m=update";
                        break;
                    default:
                }

                document.location.href = url;
            };

            $scope.goToHome = function () {
                document.location.href = "/";
            };

            $scope.load = function () {
                $scope.bootOpen("lg", "Processing", "Please wait while your request is processing.");
                var s = $location.path().split("/");
                if (s.length === 4) {
                    $scope.applicantId = parseInt(s[s.length - 1]);
                }
                $scope.initRequestor();

            };

            $scope.animationsEnabled = true;

            $scope.bootOpen = function (size, title, message) {
                var prop = { title: title, message: message };
                modalproperties.set(prop);
                var modalInstance = $modal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: "Modal.html",
                    controller: "ModalInstanceCtrl",
                    size: size
                });
                modalInstance.result.then(function () {
                }, function () {
                    $log.info("Modal dismissed at: " + new Date());
                });
            };

            $scope.open = function (size, title, message) {
                var prop = { title: title, message: message };
                modalproperties.set(prop);
                var modalInstance = $modal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: "ModalDialog.html",
                    controller: "ModalInstanceCtrl",
                    size: size
                });



                modalInstance.result.then(function () {

                }, function () {
                    $log.info("Modal dismissed at: " + new Date());
                });



            };

            $scope.toggleAnimation = function () {
                $scope.animationsEnabled = !$scope.animationsEnabled;
            };

            /* Open when someone clicks on the span element */
            $scope.openNav = function () {
                document.getElementById("myNav").style.width = "100%";
            };

            /* Close when someone clicks on the "x" symbol inside the overlay */
            $scope.closeNav = function () {
                document.getElementById("myNav").style.width = "0%";
            };

            $scope.load();



        }
    ])
.service("modalproperties", function () {
    var property = {
        title: "",
        message: ""
    };

    return {
        get: function () {
            return property;
        },
        set: function (value) {
            property = value;
        }
    };
}).filter("propsFilter", function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});

angular.module("StMarys.ctrl.pgcesecondaryapplications").controller("ModalInstanceCtrl", ["$scope", "$modalInstance", "modalproperties", function ($scope, $modalInstance, modalproperties) {

    var prop = modalproperties.get();
    $scope.title = prop.title;
    $scope.message = prop.message;

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss("cancel");
    };
}]);

angular.module("StMarys.ctrl.pgcesecondaryapplications")
.directive("randomBackgroundcolor", function () {
    return {
        restrict: "EA",
        replace: false,
        link: function (scope, element, attr) {

            var color = '#' + (Math.random() * 0xFFFFFF << 0).toString(16);
            element.css('border', '3px solid ' + color);
            element.css('border-color', color);
            element.css('border-radius', '12px');

        }
    }
});

