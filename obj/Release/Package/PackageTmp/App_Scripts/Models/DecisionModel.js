﻿var DecisionModel = function (data) {
    var self = this;
    data = data || {};

    self.id = data.Id;
    self.applicantId = data.ApplicantId;
    self.overallGrade = data.OverallGrade;
    self.acceptedConditionally = data.AcceptedConditionally;
    self.rejected = data.Rejected;
    self.conditions = data.Conditions;
    self.rejectionMain = data.RejectionMain;
    self.rejectionReason = data.RejectionReason;
    self.otherIssues = data.OtherIssues;
    self.decision = data.Decision;
    self.modifiedBy = data.ModifiedBy;
    self.createdBy = data.CreatedBy;

}