﻿var PmInterviewModel = function (data) {
    var self = this;
    data = data || {};

    self.id = data.Id;
    self.applicantId = data.ApplicantId;
    self.dateInterview = data.DateInterview;
    self.subject = data.Subject;
    self.interviewer1 = data.Interviewer1;
    self.interviewer2 = data.Interviewer2;
    self.question1Score = data.Question1Score;
    self.q1Notes = data.Q1Notes;
    self.question2Score = data.Question2Score;
    self.q2Notes = data.Q2Notes;
    self.question3Score = data.Question3Score;
    self.q3Notes = data.Q3Notes;
    self.question4Score = data.Question4Score;
    self.q4Notes = data.Q4Notes;
    self.question5Score = data.Question5Score;
    self.q5Notes = data.Q5Notes;
    self.dbsCheckIssues = data.DbsCheckIssues;
    self.dbsComment = data.DbsComment;
    self.issuesStudying = data.IssuesStudying;
    self.issuesStudyingNotes = data.IssuesStudyingNotes;
    self.physicalHealth = data.PhysicalHealth;
    self.physicalHealthNotes = data.PhysicalHealthNotes;
    self.mentalHealth = data.MentalHealth;
    self.mentalHealthNotes = data.MentalHealthNotes;
    self.personalCircumstances = data.PersonalCircumstances;
    self.personalCircumstancesNotes = data.PersonalCircumstancesNotes;
    self.religiousRequirements = data.ReligiousRequirements;
    self.religiousRequirementsNotes = data.ReligiousRequirementsNotes;
    self.failedQtsCourse = data.FailedQtsCourse;
    self.failedQtsCourseNotes = data.FailedQtsCourseNotes;
    self.dbsFitnessIssues = data.DbsFitnessIssues;
    self.dbsFitnessIssuesNotes = data.DbsFitnessIssuesNotes;
    self.questions = data.Questions;
    self.answers = data.Answers;
    self.modifiedBy = data.ModifiedBy;
    self.createdBy = data.CreatedBy;
    self.observationDays = data.ObservationDays

}