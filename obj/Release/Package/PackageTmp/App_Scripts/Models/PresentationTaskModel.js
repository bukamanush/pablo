﻿var PresentationTaskModel = function (data) {
    var self = this;
    data = data || {};

    self.id = data.Id;
    self.applicantId = data.ApplicantId;
    self.observer = data.Observer;
    self.commentary = data.Commentary;
    self.row1Score = data.Row1Score;
    self.row2Score = data.Row2Score;
    self.row3Score = data.Row3Score;
    self.row4Score = data.Row4Score;
    self.modifiedBy = data.ModifiedBy;
    self.createdBy = data.CreatedBy;

}