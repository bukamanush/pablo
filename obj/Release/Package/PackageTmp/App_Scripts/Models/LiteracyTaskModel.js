﻿var LiteracyTaskModel = function (data) {
    var self = this;
    data = data || {};

    self.id = data.Id;
    self.applicantId = data.ApplicantId;
    self.observer = data.Observer;
    self.subject = data.Subject;
    self.subjectKnowledge = data.SubjectKnowledge;
    self.subjectKnowledgeScore = data.SubjectKnowledgeScore;
    self.literacyComment = data.LiteracyComment;
    self.literacyScore1 = data.LiteracyScore1;
    self.literacyScore2 = data.LiteracyScore2;
    self.literacyScore3 = data.LiteracyScore3;
    self.literacyScore4 = data.LiteracyScore4;
    self.modifiedBy = data.ModifiedBy;
    self.createdBy = data.CreatedBy;

}