
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 09/27/2017 12:17:24
-- Generated from EDMX file: C:\RonakProject\IT.ITTInterviews\Models\IttInterviewDB.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [ITTInterviewsDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[PresentationTasks]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PresentationTasks];
GO
IF OBJECT_ID(N'[dbo].[LiteracyTasks]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LiteracyTasks];
GO
IF OBJECT_ID(N'[dbo].[Decisions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Decisions];
GO
IF OBJECT_ID(N'[dbo].[PmInterviews]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PmInterviews];
GO
IF OBJECT_ID(N'[dbo].[TeachersStandards]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TeachersStandards];
GO
IF OBJECT_ID(N'[dbo].[Applicants]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Applicants];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'PresentationTasks'
CREATE TABLE [dbo].[PresentationTasks] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ApplicantId] int  NULL,
    [Observer] nvarchar(50)  NULL,
    [Commentary] nvarchar(max)  NULL,
    [Row1Score] int  NULL,
    [Row2Score] int  NULL,
    [Row3Score] int  NULL,
    [Row4Score] int  NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [DateModified] datetime  NULL,
    [CreatedBy] nvarchar(50)  NULL,
    [DateCreated] datetime  NULL,
    [timestamp] binary(8)  NULL
);
GO

-- Creating table 'LiteracyTasks'
CREATE TABLE [dbo].[LiteracyTasks] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ApplicantId] int  NULL,
    [Observer] nvarchar(50)  NULL,
    [Subject] nvarchar(150)  NULL,
    [SubjectKnowledge] nvarchar(max)  NULL,
    [SubjectKnowledgeScore] int  NULL,
    [LiteracyComment] nvarchar(max)  NULL,
    [LiteracyScore1] int  NULL,
    [LiteracyScore2] int  NULL,
    [LiteracyScore3] int  NULL,
    [LiteracyScore4] int  NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [DateModified] datetime  NULL,
    [CreatedBy] nvarchar(50)  NULL,
    [DateCreated] datetime  NULL,
    [timestamp] binary(8)  NULL
);
GO

-- Creating table 'Decisions'
CREATE TABLE [dbo].[Decisions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ApplicantId] int  NULL,
    [OverallGrade] int  NULL,
    [AcceptedConditionally] bit  NULL,
    [Rejected] bit  NULL,
    [Conditions] nvarchar(max)  NULL,
    [RejectionMain] nvarchar(max)  NULL,
    [RejectionReason] nvarchar(max)  NULL,
    [OtherIssues] nvarchar(max)  NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [DateModified] datetime  NULL,
    [CreatedBy] nvarchar(50)  NULL,
    [DateCreated] datetime  NULL,
    [timestamp] binary(8)  NULL
);
GO

-- Creating table 'PmInterviews'
CREATE TABLE [dbo].[PmInterviews] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ApplicantId] int  NULL,
    [Subject] nvarchar(150)  NULL,
    [Interviewer1] nvarchar(50)  NULL,
    [Interviewer2] nvarchar(50)  NULL,
    [DateInterview] datetime  NULL,
    [Question1Score] int  NULL,
    [Q1Notes] nvarchar(max)  NULL,
    [Question2Score] int  NULL,
    [Q2Notes] nvarchar(max)  NULL,
    [Question3Score] int  NULL,
    [Q3Notes] nvarchar(max)  NULL,
    [Question4Score] int  NULL,
    [Q4Notes] nvarchar(max)  NULL,
    [Question5Score] int  NULL,
    [Q5Notes] nvarchar(max)  NULL,
    [DbsCheckIssues] bit  NULL,
    [DbsComment] nvarchar(max)  NULL,
    [IssuesStudying] bit  NULL,
    [IssuesStudyingNotes] nvarchar(max)  NULL,
    [PhysicalHealth] bit  NULL,
    [PhysicalHealthNotes] nvarchar(max)  NULL,
    [MentalHealth] bit  NULL,
    [MentalHealthNotes] nvarchar(max)  NULL,
    [PersonalCircumstances] bit  NULL,
    [PersonalCircumstancesNotes] nvarchar(max)  NULL,
    [ReligiousRequirements] bit  NULL,
    [ReligiousRequirementsNotes] nvarchar(max)  NULL,
    [FailedQtsCourse] bit  NULL,
    [FailedQtsCourseNotes] nvarchar(max)  NULL,
    [DbsFitnessIssues] bit  NULL,
    [DbsFitnessIssuesNotes] nvarchar(max)  NULL,
    [Questions] nvarchar(max)  NULL,
    [Answers] nvarchar(max)  NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [DateModified] datetime  NULL,
    [CreatedBy] nvarchar(50)  NULL,
    [DateCreated] datetime  NULL,
    [timestamp] binary(8)  NULL
);
GO

-- Creating table 'TeachersStandards'
CREATE TABLE [dbo].[TeachersStandards] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Observer] nvarchar(50)  NULL,
    [Applicant1Id] int  NULL,
    [Applicant1Name] nvarchar(50)  NULL,
    [Applicant1Commentary1] nvarchar(max)  NULL,
    [Applicant1Commentary2] nvarchar(max)  NULL,
    [Applicant1Row1] int  NULL,
    [Applicant1Row2] int  NULL,
    [Applicant1Row3] int  NULL,
    [Applicant1Row4] int  NULL,
    [Applicant2Id] int  NULL,
    [Applicant2Name] nvarchar(50)  NULL,
    [Applicant2Commentary1] nvarchar(max)  NULL,
    [Applicant2Commentary2] nvarchar(max)  NULL,
    [Applicant2Row1] int  NULL,
    [Applicant2Row2] int  NULL,
    [Applicant2Row3] int  NULL,
    [Applicant2Row4] int  NULL,
    [Applicant3Id] int  NULL,
    [Applicant3Name] nvarchar(50)  NULL,
    [Applicant3Commentary1] nvarchar(max)  NULL,
    [Applicant3Commentary2] nvarchar(max)  NULL,
    [Applicant3Row1] int  NULL,
    [Applicant3Row2] int  NULL,
    [Applicant3Row3] int  NULL,
    [Applicant3Row4] int  NULL,
    [Applicant4Id] int  NULL,
    [Applicant4Name] nvarchar(50)  NULL,
    [Applicant4Commentary1] nvarchar(max)  NULL,
    [Applicant4Commentary2] nvarchar(max)  NULL,
    [Applicant4Row1] int  NULL,
    [Applicant4Row2] int  NULL,
    [Applicant4Row3] int  NULL,
    [Applicant4Row4] int  NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [DateModified] datetime  NULL,
    [CreatedBy] nvarchar(50)  NULL,
    [DateCreated] datetime  NULL,
    [timestamp] binary(8)  NULL
);
GO

-- Creating table 'Applicants'
CREATE TABLE [dbo].[Applicants] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ApplicantType] nvarchar(50)  NULL,
    [Forename] nvarchar(50)  NULL,
    [Surname] nvarchar(50)  NULL,
    [UcasId] nvarchar(50)  NULL,
    [DisabilityDeclared] bit  NULL,
    [Subject] nvarchar(50)  NULL,
    [AcademicBackground] nvarchar(max)  NULL,
    [OtherALevel] nvarchar(max)  NULL,
    [AcademicSuitability] nvarchar(max)  NULL,
    [NeedSke] nvarchar(50)  NULL,
    [PersonalStatementReferences] nvarchar(max)  NULL,
    [DaysSchoolObservation] nvarchar(50)  NULL,
    [PersonalStatement] nvarchar(max)  NULL,
    [Reference1] nvarchar(50)  NULL,
    [Reference1Reaction] nvarchar(50)  NULL,
    [Reference2] nvarchar(50)  NULL,
    [Reference2Reaction] nvarchar(50)  NULL,
    [Reference3] nvarchar(50)  NULL,
    [Reference3Reaction] nvarchar(50)  NULL,
    [AppropriateReferences] bit  NULL,
    [ReferenceComment] nvarchar(max)  NULL,
    [ApplicationReviewOutcome] nvarchar(max)  NULL,
    [InterviewReject] nvarchar(50)  NULL,
    [RejectHighlight] nvarchar(max)  NULL,
    [RejectReason] nvarchar(max)  NULL,
    [ReviewedBy] nvarchar(50)  NULL,
    [TeachersStandard] bit  NULL,
    [Literacy] bit  NULL,
    [Presentation] bit  NULL,
    [InterviewPm] bit  NULL,
    [Decision] bit  NULL,
    [StatusId] int  NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [DateModified] datetime  NULL,
    [CreatedBy] nvarchar(50)  NULL,
    [DateCreated] datetime  NULL,
    [timestamp] binary(8)  NULL,
    [ReasonToInviteInterview] nvarchar(max)  NULL,
    [SKTask] bit  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'PresentationTasks'
ALTER TABLE [dbo].[PresentationTasks]
ADD CONSTRAINT [PK_PresentationTasks]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LiteracyTasks'
ALTER TABLE [dbo].[LiteracyTasks]
ADD CONSTRAINT [PK_LiteracyTasks]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Decisions'
ALTER TABLE [dbo].[Decisions]
ADD CONSTRAINT [PK_Decisions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PmInterviews'
ALTER TABLE [dbo].[PmInterviews]
ADD CONSTRAINT [PK_PmInterviews]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TeachersStandards'
ALTER TABLE [dbo].[TeachersStandards]
ADD CONSTRAINT [PK_TeachersStandards]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Applicants'
ALTER TABLE [dbo].[Applicants]
ADD CONSTRAINT [PK_Applicants]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------