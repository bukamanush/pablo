﻿var TsCandidateModel = function (data) {
    var self = this;
    data = data || {};

    self.id = data.Id;
    self.applicantId = data.ApplicantId;
    self.applicantName = data.ApplicantName;
    self.applicantCommentary1 = data.ApplicantCommentary1;
    self.applicantCommentary2 = data.ApplicantCommentary2;
    self.applicantRow1 = data.ApplicantRow1;
    self.applicantRow2 = data.ApplicantRow2;
    self.applicantRow3 = data.ApplicantRow3;
    self.applicantRow4 = data.ApplicantRow4;
    self.applicant = data.Applicant;
}